# Horse administration

Projet S.I.B.D. 03/04/2020.

**Rendu et documentation** : https://horse-administration.alexisphilip.fr/docs/

**Interface d'administration** : https://horse-administration.alexisphilip.fr

**PHPMyAdmin** : https://phpmyadmin.alexisphilip.fr

## Répertoire /

| Répertoire         | Explication                                                                                                             |
|--------------------|-------------------------------------------------------------------------------------------------------------------------|
| *queries_generate* | Autre / Génère des requêtes ([plus de détails](https://horse-administration.alexisphilip.fr/docs/#/generation-donnees)) |
| *documentation*    | Autre / Rendu et documentation                                                                                          |
| *tout le reste*    | Framework / Birdy ([architecture en détail ici](https://birdy.alexisphilip.fr/#/))                                      |

## Authors

- Alexis Philip @alexis-philip
- Nicolas Wasner @ElWesnar1
- Laura Sampaolo @Slaura

## Resources

https://horse-administration.alexisphilip.fr/docs/#/liens-et-ressources :books:
