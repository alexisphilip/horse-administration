<?php

/**
 * Database connexions.
 */

/*
 * Admins.
 */
$database["admin"] = [
    "host" => "localhost",
    "db"   => "horse-administration",
    "user" => "horse-admin",
    "pass" => "HorseAdministration"
];

$database["admin-privilege"] = [
    "host" => "localhost",
    "db"   => "horse-administration",
    "user" => "horse-admin-privilege",
    "pass" => "HorseAdministration"
];

$database["admin-contest"] = [
    "host" => "localhost",
    "db"   => "horse-administration",
    "user" => "horse-admin-contest",
    "pass" => "HorseAdministration"
];

$database["admin-columnist"] = [
    "host" => "localhost",
    "db"   => "horse-administration",
    "user" => "horse-admin-columnist",
    "pass" => "HorseAdministration"
];

/*
 * Table & index manager.
 */
$database["manager-index"] = [
    "host" => "localhost",
    "db"   => "horse-administration",
    "user" => "horse-manager-index",
    "pass" => "HorseAdministration"
];

/*
 * Automated tasks.
 */
$database["automated-task"] = [
    "host" => "localhost",
    "db"   => "horse-administration",
    "user" => "horse-automated-task",
    "pass" => "HorseAdministration"
];

/*
 * Developer.
 */
$database["dev"] = [
    "host" => "localhost",
    "db"   => "horse-administration",
    "user" => "horse-dev",
    "pass" => "HorseAdministration"
];

/*
 * Community moderator.
 */
$database["moderator"] = [
    "host" => "localhost",
    "db"   => "horse-administration",
    "user" => "horse-moderator",
    "pass" => "HorseAdministration"
];

/*
 * Horse job specialist.
 */
$database["job-specialist"] = [
    "host" => "localhost",
    "db"   => "horse-administration",
    "user" => "horse-job-specialist",
    "pass" => "HorseAdministration"
];

/*
 * Client.
 */
$database["client"] = [
    "host" => "localhost",
    "db"   => "horse-administration",
    "user" => "horse-client",
    "pass" => "HorseAdministration"
];