<?php

/**
 * Class Dashboard.
 */
class Dashboard extends MasterController
{
    /**
     * Index page.
     */
    public function index(): void
    {
        $dashboard_model = $this->loadModel("DashboardModel");

        $data = $dashboard_model->getData();
        $data["latest_newspaper"] = $dashboard_model->getLatestNewspaper();
        $data["user_logs_monthly"] = $dashboard_model->getUserLogMonthly();
        $data["total_horse_establishment_type"] = $dashboard_model->getTotalHorseEstablishmentType();

        $this->loadJS(
            "dashboard-chart-area",
            "dashboard-chart-pie"
        );

        $this->loadHelpers("DateConverter");

        $this->setTitle("Dashboard");
        $this->view("dashboard", $data);
    }
}
