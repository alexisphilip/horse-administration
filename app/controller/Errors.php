<?php

/**
 * Class Errors.
 */
class Errors extends MasterController
{
    /**
     * Error 404.
     */
    public function error404(): void
    {
        $this->view("error/404");
    }
}