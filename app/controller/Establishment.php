<?php

/**
 * Class Establishment.
 */
class Establishment extends MasterController
{
    /**
     * @var object $establishment_model EstablishmentModel's instance.
     */
    private $establishment_model;

    /**
     * Establishment constructor.
     */
    public function __construct()
    {
        $this->establishment_model = $this->loadModel("EstablishmentModel");
    }

    /**
     * Detail view.
     *
     * @param array $param URL's parameters.
     */
    public function detail(array $param): void
    {
        $data = $this->establishment_model->find($param["id"]);

        $this->setTitle("Establishment");
        $this->view("establishment_detail", $data);
    }
}