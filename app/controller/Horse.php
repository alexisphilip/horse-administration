<?php

/**
 * Class Horse.
 */
class Horse extends MasterController
{
    /**
     * @var object $horse_model HorseModel's instance.
     */
    private $horse_model;

    public function __construct()
    {
        $this->horse_model = $this->loadModel("HorseModel");
    }

    /**
     * Detail view.
     *
     * @param array $param URL's parameters.
     */
    public function detail($param): void
    {
        // Gets data from the model.
        $data["horse"] = $this->horse_model->find($param["id"]);

        $this->setTitle("Horse");
        $this->loadJS("horse-detail-chart-pie");
        $this->view("horse_detail", $data);

    }
}
