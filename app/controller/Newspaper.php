<?php

/**
 * Class Newspaper.
 */
class Newspaper extends MasterController
{
    /**
     * @var object $newspaper_model NewspaperModel's instance.
     */
    private $newspaper_model;

    /**
     * Newspaper constructor.
     */
    public function __construct()
    {
        $this->newspaper_model = $this->loadModel("NewspaperModel");
    }

    /**
     * Detail view.
     *
     * @param array $param URL's parameters.
     */
    public function detail($param): void
    {
        // Gets data from the model.
        $data = $this->newspaper_model->find($param["id"]);

        $this->setTitle("Newspaper");
        $this->view("newspaper_detail", $data);
    }
}
