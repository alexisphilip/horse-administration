<?php

/**
 * Class Orm.
 *
 * ORM's class for listing, detailing, and updating entities.
 *
 * @see ORMModel
 * @see https://horse-administration.alexisphilip.fr/docs/#/interface-administration?id=orm
 */
class Orm extends MasterController
{
    /**
     * @var object $ORM_model ORMModel's instance.
     */
    private $ORM_model;

    /**
     * Orm constructor.
     */
    public function __construct()
    {
        $this->ORM_model = $this->loadModel("ORMModel");
    }

    /**
     * Listing function.
     *
     * @param array $param URL's parameters.
     *
     * @see ORMModel
     * @see https://horse-administration.alexisphilip.fr/docs/#/interface-administration?id=ormphplist
     */
    public function list(array $param): void
    {
        $this->loadHelpers("orm_format_string");

        // If a page parameter is given.
        if (!$param["page"]) $param["page"] = 1;
        // If a limit parameter is given.
        if (!$param["limit"]) $param["limit"] = 25;
        if (!$param["where"]) $param["where"] = [1, 1];
        // If the name parameter is not given or if the table does not exist.
        if (!$param["name"]) {// || !$table_model->exists($param["name"])) {
            redirect("dashboard");
        }

        $table = ucfirst($param["name"]);
        $table_name = $param["name"];

        // Does not use the ORM's model, and uses the table's model.
        $data["table_data"] = $this->ORM_model->findAll($table_name, $param["limit"], $param["page"], $param["where"]);
        $data["table_columns"] = $this->ORM_model->getColumns($table_name);
        $data["table_name"] = $table_name;

        $this->setTitle("\"" . $table_name . "\" list");

        if ($table_name == "users") {
            $this->view($table_name, $data);
        } else {
            $this->view("orm_list", $data);
        }
    }

    /**
     * Detail function.
     *
     * @param array $param URL's parameters.
     *
     * @see ORMModel
     * @see https://horse-administration.alexisphilip.fr/docs/#/interface-administration?id=ormphpdetail
     */
    public function detail(array $param): void
    {
        $this->loadHelpers("orm_format_string");

        // If the name parameter is not given or if the table does not exist.
        if (!$param["name"]) {// || !$table_model->exists($param["name"])) {
            redirect("dashboard");
        }
        // If the ID parameter is not given.
        if (!$param["id"]) {
            redirect("dashboard");
        }

        $table = ucfirst($param["name"]);
        $table_name = $param["name"];

        $data["table_data"] = $this->ORM_model->findOne($table_name, $param["id"]);
        $data["table_name"] = $table_name;

        $this->setTitle("\"" . $table_name . "\" detail");
        $this->view("orm_detail", $data);
    }

    /**
     * Update function.
     *
     * @param array $param URL's parameters.
     *
     * @see ORMModel
     * @see https://horse-administration.alexisphilip.fr/docs/#/interface-administration?id=ormphpupdate
     */
    public function update(array $param): void
    {
        $this->loadCSS("custom-form");

        // If the name parameter is not given or if the table does not exist.
        if (!$param["name"]) {// || !$table_model->exists($param["name"])) {
            redirect("dashboard");
        }
        // If the ID parameter is not given.
        if (!$param["id"]) {
            redirect("dashboard");
        }

        $table = ucfirst($param["name"]);
        $table_name = $param["name"];

        $data["data"] = $this->ORM_model->update($table_name, $param["id"]);

        $this->loadHelpers("orm_format_string");

        $this->setTitle("\"" . $table . "\"" .  " update");
        $this->view("orm_update",$data);
    }
}
