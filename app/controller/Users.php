<?php

/**
 * Class Users.
 */
class Users extends MasterController
{
    /**
     * @var object $users_model UsersModel's instance.
     */
    private $users_model;

    /**
     * Users constructor.
     */
    public function __construct()
    {
        $this->users_model = $this->loadModel("UsersModel");
    }

    /**
     * Detail function.
     *
     * @param array $param URL's parameters.
     */
    public function detail(array $param): void
    {
        // Gets data from the model.
        $data = $this->users_model->find($param["id"]);

        $this->loadHelpers(
            "DateConverter"
        );

        $this->setTitle("Users");
        $this->view("users_detail", $data);

    }
}