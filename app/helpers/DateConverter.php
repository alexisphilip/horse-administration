<?php

/**
 * DateConverter class.
 *
 * Returns a readable time hint of the time difference between.
 * the actual date time and the given date time value.
 *
 * E.g.: If difference if 10 seconds.
 * Input: "2020-01-20 12:10:10"
 * Output: "8 seconds ago"
 *
 * E.g.: If difference if 100 seconds.
 * Input: "2020-01-20 12:10:10"
 * Output: "2 minutes ago"
 *
 * E.g.: If difference if 10.000 seconds.
 * Input: "2020-01-20 12:10:10"
 * Output: "2 hours ago"
 *
 * @author Alexis Philip
 * @see https://alexisphilip.fr/blare/we-did-it/#/php-framework
 */
class DateConverter
{
    private const supported_language_codes = ["en", "fr"];

    /**
     * Gets the time difference.
     *
     * @param string $time_stamp The creation time stamp which will be compared to.
     * @param string $language_code The country code.
     * @return string The formatted date.
     */
    public static function convert(string $time_stamp, string $language_code = "en"): string
    {
        try {
            $date = new DateTime($time_stamp);
        } catch (Exception $e) {
            p("Error: PHP could not get the current date with \"DateTime\" in \"DateConverter()\" class.", "error");
            die;
        }

        $year = $date->format("Y");
        $month = $date->format("m");
        $day = $date->format("d");

        $current_date = date("Y-m-d H:i:s");

        $diff_sec = strtotime($current_date) - strtotime($time_stamp);

        if ($diff_sec < 60) {
            $converted_date = self::formatToCountry($diff_sec, "second", $language_code);
        }
        else if ($diff_sec < 3600) {
            $converted_date = self::formatToCountry(intdiv($diff_sec, 60), "minute", $language_code);
        }
        else if ($diff_sec > 3600 && $diff_sec < 86400) {
            $converted_date = self::formatToCountry(intdiv($diff_sec, 3600), "hour", $language_code);
        }
        else if ($diff_sec > 86400 && $diff_sec < 432000) {
            $converted_date = self::formatToCountry(intdiv($diff_sec, 86400), "day", $language_code);
        }
        else {
            $whole_date = ["year" => $year, "month" => $month, "day" => $day];
            $converted_date = self::formatToCountry($whole_date, "date", $language_code);
        }

        return $converted_date;
    }

    /**
     * Formats the current time difference to a human readable difference.
     *
     * E.g.: "7 seconds ago", "1 hour ago", "4 days ago", "on Sep. 17th 2000".
     *
     * @param string|array $time The time in seconds or a date in array [M, m, d].
     * @param string $type The type of time (second, minute, hour, day or date).
     * @param string $language_code The country code.
     * @return string
     */
    private static function formatToCountry($time, string $type, string $language_code): string
    {
        $language_code = strtolower($language_code);

        $months = [
            "en" => ["January", "Febuary", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
            "fr" => ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "September", "Octobre", "Novembre", "Décembre"]
        ];


        $plural = ($time > 1 ? true : false);

        if ($language_code === "en") {
            $ago = " ago";

            switch ($type) {
                case "second":

                    $second = $plural ? "seconds" : "second";
                    $formatted_date = $time . " " . $second . " " . $ago;
                    break;
                case "minute":
                    $minute = $plural ? "minutes" : "minute";
                    $formatted_date = $time . " " . $minute . " " . $ago;
                    break;
                case "hour":
                    $hour = $plural ? "hours" : "hour";
                    $formatted_date = $time . " " . $hour . " " . $ago;
                    break;
                case "day":
                    $day = $plural ? "days" : "day";
                    $formatted_date = $time . " " . $day . " " . $ago;
                    break;
                case "date":
                    $month_str = substr($months[$language_code][$time["month"]-1], 0,3) . ".";
                    $formatted_date = "on " . $month_str . " " . $time["day"] . self::englishDay($time["day"]) . " " . $time["year"];
                    break;
            }
        } else if ($language_code === "fr") {
            $ago = "il y a ";

            switch ($type) {
                case "second":
                    $second = $plural ? "secondes" : "seconde";
                    $formatted_date = $ago . " " . $time . " " . $second;
                    break;
                case "minute":
                    $minute = $plural ? "minutes" : "minute";
                    $formatted_date = $ago . " " . $time . " " . $minute;
                    break;
                case "hour":
                    $hour = $plural ? "heures" : "heure";
                    $formatted_date = $ago . " " . $time . " " . $hour;
                    break;
                case "day":
                    $day = $plural ? "jours" : "jour";
                    $formatted_date = $ago . " " . $time . " " . $day;
                    break;
                case "date":
                    $month_str = strtolower(substr($months[$language_code][$time["month"]-1], 0,3)) . ".";
                    $formatted_date = "le " . $time["day"] . " " . $month_str . " " . $time["year"];
                    break;
            }
        } else {
            p("Error: \"DateConverter::convert()\" Language not supported yet. Supported languages: " . strtoupper(implode(", ", self::supported_language_codes)) . ".", "error");
            return "";
        }

        return $formatted_date;
    }

    /**
     * Returns a day to an short-form English suffix ("st", "rd", "st", "th").
     *
     * @param string $day The english day to be converted.
     * @return string
     */
    private function englishDay(string $day): string
    {
        $day = intval($day);

        if ($day === 1 || $day === 21 || $day === 31) {
            $formatted_day = "st";
        } else if ($day === 2 || $day === 22) {
            $formatted_day = "nd";
        } else if ($day === 3 || $day === 23) {
            $formatted_day = "rd";
        } else {
            $formatted_day = "th";
        }

        return $formatted_day;
    }
}
