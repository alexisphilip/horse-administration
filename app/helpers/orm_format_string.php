<?php

/**
 * Formats a SQL field name from it's parent table.
 *
 * E.g.:
 * Input:  "facility_item", "facility_item_type_id"
 * Output: "Type id"
 *
 * @param string $table_name The table's name.
 * @param string $field_name The field's name form the table.
 * @return string The formatted string.
 */
function orm_format_string(string $table_name, string $field_name)
{
    $formated_string = substr($field_name, strlen($table_name) + 1);

    $formated_string = orm_format_split($formated_string);

    return ucfirst($formated_string);
}

/**
 * Formats a SQL field name to print it in a news.
 *
 * E.g.:
 * Input:  "facility_item"
 * Output: "facility item"
 *
 * @param string $field_name A SQL field name.
 * @return string The formatted string.
 */
function orm_format_split(string $field_name): string
{
    $formated_string = $field_name;

    if (strpos($field_name, "_")) {
        $str_split = explode("_", $formated_string);
        $formated_string = join(" ", $str_split);
    }

    return $formated_string;
}