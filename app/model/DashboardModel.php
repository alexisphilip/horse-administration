<?php

/**
 * Class DashboardModel.
 */
class DashboardModel extends MasterModel
{
    /**
     * @var object $db PDO's instance.
     */
    private $db;

    /**
     * DashboardModel constructor.
     */
    public function __construct()
    {
        // Establishes a connection to the database and returns the PDO object.
        $this->db = $this->dbConnect("admin");
    }

    /**
     * Returns all data for the dashboard.
     *
     * @return array
     */
    public function getData(): array
    {
        // Total users.
        $sql = "SELECT COUNT(user_id) AS total_users FROM users";
        $data["total_users"] = $this->db->execQuery($sql)[0]["total_users"];

        // Total assigned facilities.
        $sql = "SELECT COUNT(establishment_facility_id) AS total_assigned_facilities FROM establishment_facility";
        $data["total_assigned_facilities"] = $this->db->execQuery($sql)[0]["total_assigned_facilities"];

        // Total horses.
        $sql = "SELECT COUNT(horse_id) AS total_horses FROM horse";
        $data["total_horses"] = $this->db->execQuery($sql)[0]["total_horses"];

        // Total assigned objects (to horses).
        $sql = "SELECT COUNT(horse_object_id) AS total_horse_objects FROM horse_object";
        $data["total_assigned_horse_objects"] = $this->db->execQuery($sql)[0]["total_horse_objects"];

        // Gets next contest.
        $current_date = date("Y-m-d H:i:s");
        $sql = "
            SELECT
                hc.hippodrome_contest_id,
                hc.hippodrome_contest_date_start,
                c.contest_name,
                e.establishment_id,
                u.user_username
            FROM hippodrome_contest hc
            LEFT JOIN contest c ON c.contest_id = hc.contest_id
            LEFT JOIN establishment e ON e.establishment_id = hc.establishment_id
            LEFT JOIN users u ON u.user_id = e.user_id
            WHERE hc.hippodrome_contest_date_start > \"$current_date\"
            ORDER BY hc.hippodrome_contest_date_start ASC
            LIMIT 1
        ";
        $data["next_contest"] = $this->db->execQuery($sql)[0];

        // Total horse disease.
        $sql = "SELECT COUNT(DISTINCT horse_id) AS total_horse_disease FROM horse_disease";
        $total_horse_disease = $this->db->execQuery($sql)[0]["total_horse_disease"];

        // Total horse parasite.
        $sql = "SELECT COUNT(DISTINCT horse_id) AS total_horse_parasite FROM horse_parasite";
        $total_horse_parasite = $this->db->execQuery($sql)[0]["total_horse_parasite"];

        // Total horse injury.
        $sql = "SELECT COUNT(DISTINCT horse_id) AS total_horse_injury FROM horse_injury";
        $total_horse_injury = $this->db->execQuery($sql)[0]["total_horse_injury"];

        $data["total_horse_disease"] = round(100 - $total_horse_disease / $data["total_horses"] * 100, 2);
        $data["total_horse_parasite"] = round(100 - $total_horse_parasite / $data["total_horses"] * 100, 2);
        $data["total_horse_injury"] = round(100 - $total_horse_injury / $data["total_horses"] * 100, 2);

        return $data;
    }

    /**
     * Returns the latest newspaper (from the current's day).
     *
     * @return array
     */
    public function getLatestNewspaper(): array
    {
        // Start date.
        $date = date("Y-m-d", strtotime("2019-03-31"));

        $sql = "
            SELECT newspaper_id
            FROM newspaper
            WHERE newspaper_release_date = \"$date\";
        ";

        $data = $this->db->execQuery($sql)[0];

        $newspaper_id = $data["newspaper_id"];

        $sql = "
            SELECT newspaper_article_title, newspaper_article_content, newspaper_article_author
            FROM newspaper_article
            WHERE newspaper_id = $newspaper_id
            LIMIT 1;
        ";

        $data["article"] = $this->db->execQuery($sql)[0];

        return $data;
    }

    /**
     * Returns the total of user logs 1 year ago to the current's day.
     *
     * @return array
     */
    public function getUserLogMonthly(): array
    {
        $month_data = [];

        // Start date.
        $time = strtotime("2019-04-01");

        // For each twelve month.
        for ($i = 0; $i < 12; $i++) {
            // Formats the date.
            $time_start = date("Y-m-d", $time);
            $month = date("m", $time);

            // Adds a month.
            $time = strtotime("+1 month", $time);

            $time_end = date("Y-m-d", $time);;

            $sql = "
                SELECT COUNT(user_log_id) AS total
                FROM user_log
                WHERE user_log_date_connection BETWEEN '$time_start' AND '$time_end'
            ";

            $total = $this->db->execQuery($sql)[0]["total"];

            $month_data[] = [$month, $total];
        }

        return $month_data;
    }

    /**
     * Returns the total horses per equestrian centers and hippodromes.
     *
     * @return array
     */
    public function getTotalHorseEstablishmentType(): array
    {
        // Total per equestrian center.
        $sql = "
            SELECT COUNT(fi.horse_id) as total_horses
            FROM facility_item fi
            LEFT JOIN establishment_facility ef	ON ef.establishment_facility_id = fi.establishment_facility_id
            LEFT JOIN establishment e ON e.establishment_id = ef.establishment_id
            WHERE
                fi.horse_id IS NOT NULL AND
                e.establishment_type_id = 1;
        ";

        $data["equestrian_center"] = $this->db->execQuery($sql)[0]["total_horses"];

        // Total per hippodrome.
        $sql = "
            SELECT COUNT(fi.horse_id) as total_horses
            FROM facility_item fi
            LEFT JOIN establishment_facility ef	ON ef.establishment_facility_id = fi.establishment_facility_id
            LEFT JOIN establishment e ON e.establishment_id = ef.establishment_id
            WHERE
                fi.horse_id IS NOT NULL AND
                e.establishment_type_id = 2;
        ";

        $data["hippodrome"] = $this->db->execQuery($sql)[0]["total_horses"];

        return $data;
    }
}