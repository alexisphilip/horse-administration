<?php

/**
 * Class EstablishmentModel.
 */
class EstablishmentModel extends MasterModel
{
    /**
     * @var object $db PDO's instance.
     */
    private $db;

    /**
     * EstablishmentModel constructor.
     */
    public function __construct()
    {
        $this->db = $this->dbConnect("admin");
    }

    /**
     * Fetches and returns a specific tuple from the ID.
     *
     * @param int $establishment_id Establishment ID.
     * @return array
     */
    public function find(int $establishment_id): array
    {
        $sql = "
            SELECT e.*, et.*, u.*
            FROM establishment e
            LEFT JOIN establishment_type et ON et.establishment_type_id = e.establishment_type_id 
            LEFT JOIN users u ON u.user_id = e.user_id
            WHERE e.establishment_id = $establishment_id
        ";

        $data["establishment"] = $this->db->execQuery($sql)[0];

        $sql = "
            SELECT ef.*,fa.*,faf.*,ent.*
            FROM establishment_facility ef
            LEFT JOIN facility fa ON fa.facility_id = ef.facility_id
            LEFT JOIN facility_family faf ON faf.facility_family_id = fa.facility_family_id
            LEFT JOIN entity_type ent ON ent.entity_type_id = faf.entity_type_id 
            WHERE ef.establishment_id = $establishment_id
        ";

        $data["facilities"] = $this->db->execQuery($sql);

        $sql = "
            SELECT COUNT(establishment_facility_id) AS total
            FROM establishment_facility
            WHERE establishment_id = $establishment_id
        ";

        $data["total_facilities"] = $this->db->execQuery($sql)[0]["total"];

        return $data;
    }
}