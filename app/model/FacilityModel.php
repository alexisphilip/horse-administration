<?php

/**
 * Class FacilityModel.
 */
class FacilityModel extends MasterModel
{
    /**
     * @var object $db PDO's instance.
     */
    private $db;

    /**
     * FacilityModel constructor..
     */
    public function __construct()
    {
        $this->db = $this->dbConnect("admin");
    }

    /**
     * Fetches and returns tuples from the given table.
     *
     * @param int $limit Limit number of tuples.
     * @param int $page Pages based on the limit of tuples.
     * @return array
     */
    public function findAll(int $limit, int $page): array
    {
        $page = $page * $limit - $limit;

        $sql = "
            SELECT f.*, ff.*, et.*
            FROM facility f
            JOIN facility_family ff ON f.facility_family_id = ff.facility_family_id
            JOIN entity_type et ON et.entity_type_id = ff.entity_type_id
            LIMIT $page, $limit;
        ";

        return $this->db->execQuery($sql);
    }
}
