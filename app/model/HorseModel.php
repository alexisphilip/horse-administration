<?php

/**
 * Class HorseModel.
 */
class HorseModel extends MasterModel
{
    /**
     * @var object $db PDO's instance.
     */
    private $db;

    /**
     * HorseModel constructor.
     */
    public function __construct()
    {
        $this->db = $this->dbConnect("admin");
    }

    /**
     * Fetches and returns a specific tuple from the given ID.
     *
     * @param int $horse_id A horse ID.
     * @return array
     */
    public function find(int $horse_id): array
    {
        // Horse data and breed name.
        $sql = "
            SELECT h.*, hb.*
            FROM horse h
            LEFT JOIN horse_breed hb ON hb.horse_breed_id = h.horse_breed_id
            WHERE h.horse_id = $horse_id
        ";

        $data = $this->db->execQuery($sql)[0];

        // Horse objects.
        $sql = "
            SELECT ho.object_id, ot.object_type_name
            FROM horse_object ho
            LEFT JOIN object o ON o.object_id = ho.object_id
            LEFT JOIN object_type ot ON ot.object_type_id = o.object_type_id
            WHERE horse_id = $horse_id
        ";

        $data["horse_objects"] = $this->db->execQuery($sql);

        // Horse diseases.
        $sql = "
            SELECT d.*
            FROM disease d
            LEFT JOIN horse_disease hd ON hd.disease_id = d.disease_id
            WHERE horse_id = $horse_id
        ";

        $data["horse_diseases"] = $this->db->execQuery($sql);

        // Horse parasites.
        $sql = "
            SELECT p.*
            FROM parasite p
            LEFT JOIN horse_parasite hp ON hp.parasite_id = p.parasite_id
            WHERE horse_id = $horse_id
        ";

        $data["horse_parasites"] = $this->db->execQuery($sql);

        // Horse injuries.
        $sql = "
            SELECT i.*
            FROM injury i
            LEFT JOIN horse_injury hi ON hi.injury_id = i.injury_id
            WHERE horse_id = $horse_id
        ";

        $data["horse_injuries"] = $this->db->execQuery($sql);

        return $data;
    }
}
