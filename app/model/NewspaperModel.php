<?php

/**
 * Class NewspaperModel.
 */
class NewspaperModel extends MasterModel
{
    /**
     * @var object $db PDO's instance.
     */
    private $db;

    /**
     * NewspaperModel constructor.
     */
    public function __construct()
    {
        $this->db = $this->dbConnect("admin");
    }

    /**
     * Fetches and returns a specific tuple from the given ID.
     *
     * @param int $newspaper_id A newspaper ID.
     * @return array
     */
    public function find($newspaper_id)
    {
        // Newspaper.
        $sql = "
            SELECT *
            FROM newspaper
            WHERE newspaper_id = $newspaper_id
        ";
        $data["newspaper"] = $this->db->execQuery($sql)[0];

        // Articles.
        $sql = "
            SELECT *
            FROM newspaper_article
            WHERE newspaper_id = $newspaper_id
        ";
        $data["articles"] = $this->db->execQuery($sql);

        // Advertisements.
        $sql = "
            SELECT na.*, a.*
            FROM newspaper_advertisement na
            LEFT JOIN advertisement a ON a.advertisement_id = na.advertisement_id
            WHERE na.newspaper_id = $newspaper_id
        ";
        $data["advertisements"] = $this->db->execQuery($sql);

        return $data;
    }
}
