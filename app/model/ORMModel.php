<?php

/**
 * Class ORMModel.
 *
 * @see Orm
 * @see https://horse-administration.alexisphilip.fr/docs/#/interface-administration?id=orm
 */
class ORMModel extends MasterModel
{
    /**
     * @var object $db PDO's instance.
     */
    private $db;

    /**
     * ORMModel constructor.
     */
    public function __construct()
    {
        $this->db = $this->dbConnect("admin");
    }

    /**
     * Fetches and returns tuples from the given table.
     *
     * @param string $table_name Name of the table.
     * @param int $limit Limit number of tuples.
     * @param int $page Pages based on the limit of tuples.
     * @param array $where Array containing [column_name, value] to add a WHERE clause to the query.
     * @return array
     *
     * @see Orm
     * @see https://horse-administration.alexisphilip.fr/docs/#/interface-administration?id=ormmodelphpfindall
     */
    public function findAll(string $table_name, int $limit, int $page, array $where): array
    {
        $page = $page * $limit - $limit;
        $where = "AND $where[0] = $where[1]";

        $sql = "
            SELECT *
            FROM $table_name
            WHERE 1 $where
            LIMIT $page, $limit; 
        ";

        return $this->db->execQuery($sql);
    }

    /**
     * Fetches and returns a specific tuple from the given table and ID.
     *
     * @param string $table_name Name of the table.
     * @param int $id A PK's ID.
     * @return array
     *
     * @see Orm
     * @see https://horse-administration.alexisphilip.fr/docs/#/interface-administration?id=ormmodelphpfindone
     */
    public function findOne(string $table_name, int $id): array
    {
        $field_id = $table_name . "_id";

        $sql = "
            SELECT *
            FROM $table_name
            WHERE $field_id = $id
        ";

        return $this->db->execQuery($sql)[0];
    }

    /**
     * Fetches and returns table's information to build a specific update form to modify the selected tuple.
     *
     * @param string $table_name Name of the table.
     * @param int $id A PK's ID.
     * @return array
     *
     * @see Orm
     * @see https://horse-administration.alexisphilip.fr/docs/#/interface-administration?id=ormmodelphpupdate
     */
    public function update(string $table_name, int $id): array
    {
        $database_name = "alexisphilip-test_si_bd";

        $sql = "
            SHOW COLUMNS
            FROM $table_name
        ";

        $columns = $this->db->execQuery($sql);

        // Checks if field is a foreign key.
        $foreign_keys = [];
        for ($i = 0; $i < count($columns); $i++) {
            $col_name = $columns[$i]["Field"];

            // If field is a foreign key.
            if (substr($col_name, -3) == "_id" && $i !== 0) {
                $columns[$i]["foreign_key"] = true;
            } // If the field is not a foreign key.
            else {
                $columns[$i]["foreign_key"] = false;
            }
        }

        $data = [];

        // For each columns.
        foreach ($columns as $col) {

            $col_data = [];
            $option = [];

            // The current's field.
            $name = $col["Field"];
            // The table's PK.
            $pk = $table_name . "_id";

            // Gets the values.
            $sql = "
                SELECT $name
                FROM $table_name
                WHERE $pk = $id
            ";

            $value = $this->db->execQuery($sql)[0][$name];

            // If the column is a foreign key, fetch all of its IDs.
            if ($col["foreign_key"]) {
                $foreign_table = substr($name, 0, -3);
                $foreign_keys[] = $name;

                // Get the foreign's key IDs from the table.
                $sql = "
                    SELECT COUNT(DISTINCT $name) as total
                    FROM $foreign_table
                ";

                $results = $this->db->execQuery($sql)[0]["total"];

                // If there is under 999 results.
                if ($results < 999) {

                    // Get the results.
                    $sql = "
                        SELECT $name
                        FROM $foreign_table
                        ORDER BY $name
                    ";

                    $results = $this->db->execQuery($sql);

                    // Parses each value from the SQL Std Object to a normal array.
                    foreach ($results as $result) {
                        $option[] = $result[$name];
                    }

                    $type = "int";
                    $range = false;
                } // Will get the range of ID field later, to not display too many IDs.
                else {
                    $result = $this->getFieldType($col);

                    $type = $result["type"];
                    $range = $result["range"];
                    $option = false;
                }
            } // If the field is not a foreign key.
            else {
                $result = $this->getFieldType($col);

                $type = $result["type"];
                $range = $result["range"];
                $option = false;
            }

            $col_data["name"] = $name;
            $col_data["value"] = $value;
            $col_data["type"] = $type;
            $col_data["option"] = $option;
            $col_data["range"] = $range;

            $data["table_data"][] = $col_data;
        }

        $data["table_name"] = $table_name;

        return $data;
    }

    /**
     * Returns the given field type and it's range.
     *
     * E.g.:
     * input : ["Type" => "varchar(255)"]
     * output: ["string", [1, 255]]
     *
     * E.g.:
     * input : ["Type" => "int(100)"]
     * output: ["int", [1, 999]]
     *
     * @param array $field
     * @return array
     */
    public function getFieldType(array $field): array
    {
        $type_raw = $field["Type"];

        // Gets the type's string.
        preg_match("/([a-z])*/", $type_raw, $type);

        if (in_array($type[0], ["int", "tinyint"])) {
            $type = "int"; // Has a defined length.
        } else if (in_array($type[0], ["varchar", "char"])) {
            $type = "string"; // Has a defined length.
        } else {
            $type = "text"; // Does not have a defined length.
        }

        // If type is "int" or "string", we can calculate a range.
        if ($type == "int" || $type == "string") {
            // Gets the type's size.
            preg_match("/(\(.*\))/", $type_raw, $type_size);

            $type_size = substr($type_size[0], 1, -1);

            if ($type == "int") {
                // Type's max size.
                $range_max_val = pow(10, $type_size) - 1;
                // If field is an ID, cannot have 0 min value.
                if (substr($field["Field"], -3) == "_id")
                    $range_min_val = 1;
                else
                    $range_min_val = 0;
            } else {
                $range_max_val = $type_size;
                $range_min_val = 1;
            }
            $range = [$range_min_val, $range_max_val];
        } // If "textarea", we cannot find a range.
        else {
            $type = "string";
            $range = false;
        }

        return ["type" => $type, "range" => $range];
    }

    /**
     * Return columns from the given table.
     *
     * @param string $table_name The table's name.
     * @return array
     *
     * @see Orm
     */
    public function getColumns($table_name): array
    {
        $sql = "
            SHOW COLUMNS
            FROM $table_name
        ";

        return $this->db->execQuery($sql);
    }
}
