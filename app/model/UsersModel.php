<?php

/**
 * Class UsersModel.
 */
class UsersModel extends MasterModel
{
    /**
     * @var object $db PDO's instance.
     */
    private $db;

    /**
     * UsersModel constructor.
     */
    public function __construct()
    {
        $this->db = $this->dbConnect("admin");
    }

    /**
     * Fetches and returns a specific tuple from the given ID.
     *
     * @param int $users_id A user ID.
     * @return array
     */
    public function find($users_id): array
    {
        $sql = "
            SELECT u.*
            FROM users u
            WHERE u.user_id = $users_id
        ";

        $data = $this->db->execQuery($sql)[0];

        $sql = "
            SELECT u.user_id, h.horse_name, fi.horse_id, h.horse_experience 
            FROM users u
            LEFT JOIN establishment e ON e.user_id = u.user_id
            LEFT JOIN establishment_facility ef ON ef.establishment_id = e.establishment_id
            LEFT JOIN facility_item fi ON fi.establishment_facility_id = ef.establishment_facility_id
            LEFT JOIN horse h ON h.horse_id = fi.horse_id
            WHERE
                u.user_id = $users_id AND
                fi.horse_id IS NOT NULL
            ORDER BY h.horse_experience DESC
            LIMIT 5
        ";

        $data["user_horses"] = $this->db->execQuery($sql);

        $sql = "
            SELECT COUNT(DISTINCT fi.horse_id) as total
            FROM users u
            LEFT JOIN establishment e ON e.user_id = u.user_id
            LEFT JOIN establishment_facility ef ON ef.establishment_id = e.establishment_id
            LEFT JOIN facility_item fi ON fi.establishment_facility_id = ef.establishment_facility_id
            WHERE u.user_id = $users_id AND fi.horse_id IS NOT NULL
        ";

        $data["total_horses"] = $this->db->execQuery($sql)[0]["total"];

        $sql = "
            SELECT u.user_id, log.user_log_id, log.user_log_ip, log.user_log_date_connection
            FROM users u
            LEFT JOIN user_log log ON log.user_id = u.user_id
            WHERE u.user_id = $users_id
            ORDER BY log.user_log_date_connection DESC
            LIMIT 5
        ";

        $data["user_log"] = $this->db->execQuery($sql);


        $sql = "
            SELECT e.establishment_id, e.establishment_price, et.establishment_type_name, COUNT(ef.establishment_facility_id) as total_facilities
            FROM establishment e
            LEFT JOIN establishment_facility ef ON ef.establishment_id = e.establishment_id
            LEFT JOIN establishment_type et ON et.establishment_type_id = e.establishment_type_id
            WHERE e.user_id = $users_id
            GROUP BY e.establishment_id
            ORDER BY e.establishment_price DESC
            LIMIT 5
        ";

        $data["user_establishments"] = $this->db->execQuery($sql);

        $sql = "
            SELECT COUNT(DISTINCT ef.facility_id) as total
            FROM users u
            LEFT JOIN establishment e ON e.user_id = u.user_id
            LEFT JOIN establishment_facility ef ON ef.establishment_id = e.establishment_id
            WHERE u.user_id = $users_id
        ";

        $data["total_facilities"] = $this->db->execQuery($sql)[0]["total"];

        return $data;
    }
}