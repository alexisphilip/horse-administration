<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
    <a href="<?= base_url() ?>orm/detail/name=hippodrome_contest&id="></a>
    <div>
        <a href="https://horse-administration.alexisphilip.fr/docs/" class="d-none d-sm-inline-block btn btn-md btn-info shadow-sm mr-3">
            <i class="fas fa-link fa-sm text-white-50 mr-1"></i> Rendu / documentation
        </a>
        <a href="https://phpmyadmin.alexisphilip.fr/" class="d-none d-sm-inline-block btn btn-md btn-primary shadow-sm">
            <i class="fas fa-database fa-sm text-white-50 mr-1"></i> PHPMyAdmin
        </a>
    </div>
</div>

<!-- Content Row -->
<div class="row">

    <!-- Earnings (Monthly) Card Example -->
    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-primary shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Users
                        </div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= number_format($total_users) ?></div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-user fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Earnings (Monthly) Card Example -->
    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-success shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Horses
                        </div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= number_format($total_horses) ?></div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-horse-head fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Pending Requests Card Example -->
    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-warning shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Assigned horse objects
                        </div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= number_format($total_assigned_horse_objects) ?></div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-object-group fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Earnings (Monthly) Card Example -->
    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-info shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Assigned facilities
                        </div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= number_format($total_assigned_facilities) ?></div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-industry fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Content Row -->

<div class="row">

    <!-- Area Chart -->
    <div class="col-xl-8 col-lg-7">
        <div class="card shadow mb-4">
            <!-- Card Header - Dropdown -->
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 text-primary">Annual user <a class="font-weight-bolder" href="<?= base_url() ?>orm/list/name=user_log">logging activity</a></h6>
            </div>
            <!-- Card Body -->
            <div class="card-body">
                <div class="chart-area">
                    <canvas id="myAreaChart" data-user_log='<?= json_encode($user_logs_monthly) ?>'></canvas>
                </div>
            </div>
        </div>
    </div>

    <!-- Pie Chart -->
    <div class="col-xl-4 col-lg-5">
        <div class="card shadow mb-4">
            <!-- Card Header - Dropdown -->
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 text-primary">Total horses per <a class="font-weight-bolder" href="<?= base_url() ?>orm/list/name=establishment_type">establishment types</a></h6>
            </div>
            <!-- Card Body -->
            <div class="card-body">
                <div class="chart-pie pt-4 pb-2">
                    <canvas id="myPieChart" data-chart='<?= json_encode($total_horse_establishment_type) ?>'></canvas>
                </div>
                <div class="mt-4 text-center small">
                    <span class="mr-2">
                      <i class="fas fa-circle text-info"></i> Equestrian center
                    </span>
                    <span class="mr-2">
                      <i class="fas fa-circle text-danger"></i> Hippodrome
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Content Row -->
<div class="row">

    <!-- Content Column -->
    <div class="col-lg-6 mb-4">

        <!-- Project Card Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Horses health state</h6>
            </div>
            <div class="card-body">
                <h4 class="small">Horses without <a class="font-weight-bold" href="<?= base_url() ?>orm/list/name=disease">diseases</a> <span
                            class="float-right"><?= $total_horse_disease ?>%</span>
                </h4>
                <div class="progress mb-4">
                    <div class="progress-bar bg-info" role="progressbar" style="width: <?= $total_horse_disease ?>%"
                         aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <h4 class="small">Horses without <a class="font-weight-bold" href="<?= base_url() ?>orm/list/name=parasite">parasites</a><span
                            class="float-right"><?= $total_horse_parasite ?>%</span>
                </h4>
                <div class="progress mb-4">
                    <div class="progress-bar bg-warning" role="progressbar" style="width: <?= $total_horse_parasite ?>%"
                         aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <h4 class="small">Horses without <a class="font-weight-bold" href="<?= base_url() ?>orm/list/name=injury">injuries</a> <span
                            class="float-right"><?= $total_horse_injury ?>%</span></h4>
                <div class="progress mb-4">
                    <div class="progress-bar bg-danger" role="progressbar" style="width: <?= $total_horse_injury ?>%"
                         aria-valuenow="60"
                         aria-valuemin="0" aria-valuemax="100"></div>
                </div>
            </div>
        </div>

    </div>

    <div class="col-lg-6 mb-4">

        <!-- Illustrations -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <div class="d-flex align-items-center font-weight-bold text-primary m-0">
                    <i class="fas fa-fw fa-medal mr-1"></i>
                    <span class="badge badge-pill badge-danger mr-2">NEWS</span>
                    <p class="m-0 p-0">Next contest on <strong><?= $next_contest["hippodrome_contest_date_start"] ?></strong></p>
                </div>
            </div>
            <div class="card-body">
                <p>Checkout
                    <a href="<?= base_url() ?>orm/detail/name=hippodrome_contest&id=<?= $next_contest["hippodrome_contest_id"] ?>"><?= $next_contest["contest_name"] ?>
                        's
                        contest</a> at
                    <a href="<?= base_url() ?>establishment/detail/id=<?= $next_contest["establishment_id"] ?>"><?= $next_contest["user_username"] ?>
                        's hippdrome!</a>
                </p>
                <a href="<?= base_url() ?>orm/detail/name=hippodrome_contest&id=<?= $next_contest["hippodrome_contest_id"] ?>">See
                    contest &rarr;</a>
            </div>
        </div>

        <!-- Approach -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <div class="d-flex align-items-center font-weight-bold text-primary m-0">
                    <i class="fas fa-fw fa-pencil-alt mr-1"></i>
                    <span class="badge badge-pill badge-danger mr-2">NEWS</span>
                    <p class="m-0 p-0">Lastest newspaper n°<a class="font-weight-bolder" href="<?= base_url() ?>newspaper/detail/id=<?= $latest_newspaper["newspaper_id"] ?>">3249</a></p>
                </div>
            </div>
            <div class="card-body">
                <h4><?= $latest_newspaper["article"]["newspaper_article_title"] ?></h4>
                <p class="mb-0">Article by <strong><?= $latest_newspaper["article"]["newspaper_article_author"] ?></strong></p><br>
                <p><?= substr($latest_newspaper["article"]["newspaper_article_content"], 0, 250) ?>...</p>
                <a href="<?= base_url() ?>newspaper/detail/id=<?= $latest_newspaper["newspaper_id"] ?>">Read of it &rarr;</a>
            </div>
        </div>

    </div>
</div>


<!-- Page level plugins -->
<script src="<?= vendor_url("chart.js/Chart.min.js") ?>"></script>