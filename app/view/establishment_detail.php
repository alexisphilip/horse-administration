<div class="container-fluid">

    <div class="row">

        <div class="col-xl-8 col-lg-7">

            <div class="card shadow pt-4 px-4 mb-4">
                <h1><?= $establishment["establishment_type_name"] ?></h1>
                <div class="d-flex justify-content-between align-items-center">
                    <p><?= $user_f_name ?> <?= $user_l_name ?></p>
                    <?php $split = explode(" ", $user_creation_date); ?>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6 col-md-5">
                    <div class="card border-left-success shadow mb-4">
                        <div class="card-body pb-2">
                            <div class="d-flex justify-content-between align-items-center">
                                <h3 class="text-lg font-weight-bold text-success">Total capacity</h3>
                                <h3><strong><?= number_format($establishment["establishment_capacity"]) ?></strong></h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-5">
                    <div class="card border-left-danger shadow mb-4">
                        <div class="card-body pb-2">
                            <div class="d-flex justify-content-between align-items-center">
                                <h3 class="text-lg font-weight-bold text-warning">Price</h3>
                                <h3><strong>$<?= number_format($establishment["establishment_price"]) ?></strong></h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="col-xl-4 col-lg-5">

            <div class="card border-left-info shadow mb-4" style="border-width: 3px">
                <!-- Card Header -->
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-info">Owner information</h6>
                </div>
                <!-- Card Content -->
                <div class="card-body pb-0">
                    <table style="width: 100%;" class="table">
                        <tr>
                            <th style="border-top: none;">Username</th>
                            <td style="border-top: none;" class="text-right">
                                <a href="<?= base_url() ?>users/detail/id=<?= $establishment["user_id"] ?>"><?= $establishment["user_username"] ?></a>
                            </td>
                        </tr>
                        <tr>
                            <th>Name</th>
                            <td class="text-right"><?= $establishment["user_f_name"] ?> <?= $establishment["user_l_name"] ?></td>
                        </tr>
                        <tr>
                            <th>User balance</th>
                            <td class="text-right" style="overflow: hidden">
                                $<?= number_format($establishment["user_balance"]) ?></td>
                        </tr>
                    </table>
                </div>
            </div>

        </div>

    </div>

    <div class="row">
        <div class="col">
            <div class="card border-left-info shadow mb-4">
                <!-- Card Header - Accordion -->
                <div class="d-block card-header py-3">
                    <h6 class="m-0 font-weight-bold text-info">Assigned facilities to this establishment
                        (<?= $facility["total_facilities"] ?>)</h6>
                </div>
                <!-- Card Content - Collapse -->
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <tr>
                                <th style="border-top: none;">Id</th>
                                <th style="border-top: none;">Family Name</th>
                                <th style="border-top: none;">Capacity</th>
                                <th style="border-top: none;">Level</th>
                                <th style="border-top: none;">Price</th>
                                <th style="border-top: none;">Entity type</th>
                            </tr>
                            <?php foreach ($facilities as $value) { ?>
                                <tr>
                                    <td>
                                        <a href="<?= base_url() . "orm/detail/name=facility&id=" . $value["facility_id"] ?>"><?= $value["facility_id"] ?></a>
                                    </td>
                                    <td>
                                        <a href="<?= base_url() . "orm/detail/name=facility_family&id=" . $value["facility_family_id"] ?>"><?= $value["facility_family_name"] ?></a>
                                    </td>
                                    <td><?= number_format($value["facility_capacity"]) ?> </td>
                                    <td><?= $value["facility_level"] ?> </td>
                                    <td>$<?= number_format($value["facility_price"]) ?></td>
                                    <td>
                                        <a href="<?= base_url() . "orm/detail/name=entity_type&id=" . $value["entity_type_id"] ?>"><?= $value["entity_type_name"] ?></a>
                                    </td>
                                </tr>
                            <?php } ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Page level plugins -->
<script src="<?= vendor_url("chart.js/Chart.min.js") ?>"></script>