<?php
foreach ($data

as $value) {
$id = $value['horse_id'];
$breed_id = $value['horse_breed_id'];
$name = $value['horse_name'];
$resistance = $value['horse_resistance'];
$endurance = $value['horse_endurance'];
$explosive_power = $value['horse_explosive_power'];
$speed = $value['horse_speed'];
$sociability = $value['horse_sociability'];
$intelligence = $value['horse_intelligence'];
$temperament = $value['horse_temperament'];
$health_state = $value['horse_health_state'];
$moral_state = $value['horse_moral_state'];
$stress_state = $value['horse_stress_state'];
$exhaustion_state = $value['horse_exhaustion_state'];
$hunger_state = $value['horse_hunger_state'];
$cleanliness_state = $value['horse_cleanliness_state'];
$experience = $value['horse_experience'];
$level = $value['horse_level'];
$overall_state = $value['horse_overall_state'];
$breed_name = $value['horse_breed_name'];
$breed_description = $value['horse_breed_description'];
$objects = $value['horse_objects'];
$xp = ($experience / 10000) * 100;
?>

<div class="container-fluid">
    <div class="row">
        <div class="col-xl-8 col-lg-7">

            <div class="card shadow pt-4 px-4 mb-4">
                <h1><?= $value["horse_name"] ?></h1>
                <p>Breed: <a
                            href="<?= base_url() . "orm/detail/name=horse_breed&id=" . $value["horse_breed_id"] ?>"><?= $value["horse_breed_name"] ?></a>
                </p>
            </div>

            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Psychology</h6>
                </div>
                <div class="card-body">
                    <h4 class="small font-weight-bold">Sociability <span
                                class="float-right"><?= $sociability ?> / 10</span>
                    </h4>
                    <div class="progress mb-4">
                        <div class="progress-bar bg-danger" role="progressbar"
                             style="width: <?= ($sociability * 10) ?>%" aria-valuenow="20" aria-valuemin="0"
                             aria-valuemax="100"></div>
                    </div>
                    <h4 class="small font-weight-bold">Intelligence <span
                                class="float-right"><?= $intelligence ?> / 10</span>
                    </h4>
                    <div class="progress mb-4">
                        <div class="progress-bar bg-warning" role="progressbar"
                             style="width: <?= ($intelligence * 10) ?>%" aria-valuenow="40" aria-valuemin="0"
                             aria-valuemax="100"></div>
                    </div>
                    <h4 class="small font-weight-bold">Temperament <span
                                class="float-right"><?= $temperament ?> / 10</span></h4>
                    <div class="progress mb-4">
                        <div class="progress-bar" role="progressbar" style="width: <?= ($temperament * 10) ?>%"
                             aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>
            </div>

        </div>

        <div class="col-xl-4 col-lg-5">
            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Characteristics</h6>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                    <div class="chart-pie pt-4">
                        <canvas id="CaracteristicsHorse"
                                data-chart="<?= $resistance . "," . $endurance . "," . $explosive_power . "," . $speed ?>"></canvas>
                    </div>
                    <hr>
                    <span class="mr-2">
                          <i class="fas fa-circle text-primary"></i> Resistance
                        </span>
                    <span class="mr-2">
                          <i class="fas fa-circle text-success"></i> Endurance
                        </span>
                    <span class="mr-2">
                          <i class="fas fa-circle text-info"></i> Explosive power
                        </span>
                    <span class="mr-2">
                          <i class="fas fa-circle text-danger"></i> Speed
                        </span>
                </div>
            </div>
        </div>
    </div>

    <!-- Row experience & level -->
    <div class="row">
        <div class="col-md mb-4">
            <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                Experience
                            </div>
                            <div class="row no-gutters align-items-center">
                                <div class="col-auto">
                                    <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800"><?= $experience ?></div>
                                </div>
                                <div class="col">
                                    <div class="progress progress-sm mr-2">
                                        <div class="progress-bar bg-success" role="progressbar"
                                             style="width: <?= $xp ?>%" aria-valuenow="<?= $experience ?>"
                                             aria-valuemin="0" aria-valuemax="10000"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-bolt fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md mb-4">
            <div class="card border-left-info shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Level</div>
                            <div class="row no-gutters align-items-center">
                                <div class="col-auto">
                                    <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800"><?= $level ?></div>
                                </div>
                                <div class="col">
                                    <div class="progress progress-sm mr-2">
                                        <div class="progress-bar bg-info" role="progressbar"
                                             style="width: <?= $level ?>%" aria-valuenow="<?= $level ?>"
                                             aria-valuemin="1" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-democrat fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md mb-4">
            <div class="card border-left-info shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Overall state</div>
                            <div class="row no-gutters align-items-center">
                                <div class="col-auto">
                                    <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800"><?= $overall_state ?></div>
                                </div>
                                <div class="col">
                                    <div class="progress progress-sm mr-2">
                                        <div class="progress-bar bg-info" role="progressbar"
                                             style="width: <?= $overall_state * 10 ?>%"
                                             aria-valuenow="<?= $overall_state ?>"
                                             aria-valuemin="1" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-auto d-flex align-items-center">
                            <i class="fas fa-smile fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-sm">
            <div class="card shadow mb-4">
                <!-- Card Header - Accordion -->
                <a href="#collapseCardState" class="d-block card-header py-3" data-toggle="collapse" role="button"
                   aria-expanded="true" aria-controls="collapseCardExample">
                    <h6 class="m-0 font-weight-bold text-primary">State information</h6>
                </a>
                <!--Card Content - Collapse-->
                <div class="collapse show" id="collapseCardState">
                    <div class="card-body d-flex flex-wrap justify-content-center">

                        <div class="text-center mx-5">
                            <h3><strong><?= $health_state ?> / 100</strong></h3>
                            <p>Health</p>
                        </div>
                        <div class="text-center mx-5">
                            <h3><strong><?= $moral_state ?> / 10</strong></h3>
                            <p>Moral</p>
                        </div>
                        <div class="text-center mx-5">
                            <h3><strong><?= $stress_state ?> / 10</strong></h3>
                            <p>Stress</p>
                        </div>
                        <div class="text-center mx-5">
                            <h3><strong><?= $exhaustion_state ?> / 10</strong></h3>
                            <p>Exhaustion</p>
                        </div>
                        <div class="text-center mx-5">
                            <h3><strong><?= $hunger_state ?> / 10</strong></h3>
                            <p>Hunger</p>
                        </div>
                        <div class="text-center mx-5">
                            <h3><strong><?= $cleanliness_state ?> / 10</strong></h3>
                            <p>Cleanliness</p>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="card shadow mb-4">
                <!-- Card Header - Accordion -->
                <a href="#collapseCardObjects" class="d-block card-header py-3" data-toggle="collapse" role="button"
                   aria-expanded="true" aria-controls="collapseCardExample">
                    <h6 class="m-0 font-weight-bold text-primary">Assigned objects (<?= count($objects) ?>)</h6>
                </a>
                <!--Card Content - Collapse-->
                <div class="collapse show" id="collapseCardObjects">
                    <div class="card-body pb-0">
                        <?php foreach ($objects as $object) { ?>
                            <p>
                                <a href="<?= base_url() . "object/detail/id=" . $object["object_id"] ?>"><?= ucfirst($object['object_type_name']) ?></a>
                            </p>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Horse health state -->
    <div class="row">
        <div class="col-lg">
            <div class="card shadow pt-4 pb-2 px-4 mb-4">
                <h4>General medical data</h4>
            </div>
        </div>
    </div>
    <div class="row">

        <div class="col-md">
            <div class="card shadow mb-4">
                <!-- Card Header - Accordion -->
                <a href="#horseDisease" class="d-block card-header py-3" data-toggle="collapse" role="button"
                   aria-expanded="true" aria-controls="collapseCardExample">
                    <h6 class="m-0 font-weight-bold text-primary">Diseases (<?= count($value["horse_diseases"]) ?>)</h6>
                </a>
                <!--Card Content - Collapse-->
                <div class="collapse show" id="horseDisease">
                    <div class="card-body pb-0">
                        <?php
                        if ($value["horse_diseases"]) {
                            foreach ($value["horse_diseases"] as $disease) { ?>
                                <p>
                                    <a href="<?= base_url() . "orm/detail/name=disease&id=" . $disease["disease_id"] ?>"><?= $disease["disease_name"] ?></a>
                                </p>
                            <?php }
                        } else { ?>
                            <p>This horse does not have any <a href="<?= base_url() . "orm/list/name=disease" ?>">diseases</a>.
                                Great!</p>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md">
            <div class="card shadow mb-4">
                <!-- Card Header - Accordion -->
                <a href="#horseParasites" class="d-block card-header py-3" data-toggle="collapse" role="button"
                   aria-expanded="true" aria-controls="collapseCardExample">
                    <h6 class="m-0 font-weight-bold text-primary">Parasites (<?= count($value["horse_parasite"]) ?>)</h6>
                </a>
                <!--Card Content - Collapse-->
                <div class="collapse show" id="horseParasites">
                    <div class="card-body pb-0">
                        <?php
                        if ($value["horse_parasites"]) {
                            foreach ($value["horse_parasites"] as $disease) { ?>
                                <p>
                                    <a href="<?= base_url() . "orm/detail/name=parasite&id=" . $disease["parasite_id"] ?>"><?= $disease["parasite_name"] ?></a>
                                </p>
                            <?php }
                        } else { ?>
                            <p>This horse does not have any <a href="<?= base_url() . "orm/list/name=parasite" ?>">parasites</a>.
                                Great!</p>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md">
            <div class="card shadow mb-4">
                <!-- Card Header - Accordion -->
                <a href="#horseInjuries" class="d-block card-header py-3" data-toggle="collapse" role="button"
                   aria-expanded="true" aria-controls="collapseCardExample">
                    <h6 class="m-0 font-weight-bold text-primary">Injuries (<?= count($value["horse_injury"]) ?>)</h6>
                </a>
                <!--Card Content - Collapse-->
                <div class="collapse show" id="horseInjuries">
                    <div class="card-body pb-0">
                        <?php
                        if ($value["horse_injuries"]) {
                            foreach ($value["horse_injuries"] as $disease) { ?>
                                <p>
                                    <a href="<?= base_url() . "orm/detail/name=injury&id=" . $disease["injury_id"] ?>"><?= $disease["injury_name"] ?></a>
                                </p>
                            <?php }
                        } else { ?>
                            <p>This horse does not have any <a href="<?= base_url() . "orm/list/name=injury" ?>">injuries</a>.
                                Great!</p>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <?php } ?>

    <!-- Page level plugins -->
    <script src="<?= vendor_url("chart.js/Chart.min.js") ?>"></script>
