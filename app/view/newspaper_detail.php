<div class="container-fluid">
    <div class="row">
        <div class="col-xl-8 col-lg-7">

            <div class="card shadow pt-4 pb-3 px-4 mb-4">
                <h1>Newspaper n° <?= $newspaper["newspaper_id"] ?></h1>
                <h6>Published on <strong><?= $newspaper["newspaper_release_date"] ?></strong></h6>
            </div>

            <div class="card shadow mb-4">
                <div class="card-body p-5">
                    <?php foreach ($articles as $value) { ?>
                        <h3><?= $value["newspaper_article_title"] ?></h3>
                        <p class="mt-4">Article by <strong><?= $value["newspaper_article_author"] ?></strong></p><br>
                        <p><?= $value["newspaper_article_content"] ?></p>
                        <hr class="my-5">
                    <?php } ?>
                </div>
            </div>

        </div>

        <div class="col-xl-4 col-lg-5">

            <?php foreach ($advertisements as $value) { ?>

                <div class="card border-left-success shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Advertisement</h6>
                    </div>
                    <div class="card-body">
                        <h3><?= $value["advertisement_title"] ?></h3>
                        <p class="mb-0">Advertisement by <strong><?= $value["advertisement_title"] ?></strong></p><br>
                        <p><?= $value["advertisement_content"] ?></p>
                    </div>
                </div>

            <?php } ?>

        </div>
    </div>
</div>

<!-- Page level plugins -->
<script src="<?= vendor_url("chart.js/Chart.min.js") ?>"></script>
