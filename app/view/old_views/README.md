# Star Views: The Old Views and The New ORM

A long time ago, in a `framework without ORM` far, far away...

The old views were used when the ORM was not created yet. Back in the days, where the ORM Jedi were not born yet,
a lone programmer build all these views. Yes, **the 33 of them**. 

Now that the ORM Jedi have come to save the galaxy, the old views remain deprecated.

The end?

![](../../../assets/img/star-wars-orm.jpg)
 