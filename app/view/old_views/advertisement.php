<!-- Page Heading -->
<h1 class="h3 mb-3 text-gray-800">Advertisements</h1>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Advertisements</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Title</th>
                    <th>Content</th>
                    <th>Company</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th>ID</th>
                    <th>Title</th>
                    <th>Content</th>
                    <th>Company</th>
                </tr>
                </tfoot>
                <tbody>
                <?php foreach ($advertisement as $key => $value) { ?>
                    <tr>
                        <td><a href="<?= base_url() ?>orm/detail/name=advertisement&id=<?= $value["advertisement_id"] ?>"><?= $value["advertisement_id"] ?></a></td>
                        <td><?= $value["advertisement_title"] ?></td>
                        <td><?= $value["advertisement_content"] ?></td>
                        <td><?= $value["advertisement_company"] ?></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>


<!-- Page level plugins -->
<script src="<?= vendor_url("datatables/jquery.dataTables.min.js") ?>"></script>
<script src="<?= vendor_url("datatables/dataTables.bootstrap4.min.js") ?>"></script>

<!-- Page level custom scripts -->
<script src="<?= js_url("demo/datatables-demo") ?>"></script>