<!-- Page Heading -->
<h1 class="h3 mb-3 text-gray-800">Contest prizes</h1>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Contest prizes</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Assigned contest ID</th>
                    <th>Object ID</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th>ID</th>
                    <th>Assigned contest ID</th>
                    <th>Object ID</th>
                </tr>
                </tfoot>
                <tbody>
                <?php foreach ($contest_prize as $key => $value) { ?>
                    <tr>
                        <td><a href="<?= base_url() ?>orm/detail/name=contest_prize&id=<?= $value["contest_prize_id"] ?>"><?= $value["contest_prize_id"] ?></a></td>
                        <td><a href="<?= base_url() . "orm/detail/name=hippodrome_contest&id=" . $value["hippodrome_contest_id"] ?>"><?= $value["hippodrome_contest_id"] ?></a></td>
                        <td><a href="<?= base_url() . "orm/detail/name=object&id=" . $value["object_id"] ?>"><?= $value["object_id"] ?></a></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>


<!-- Page level plugins -->
<script src="<?= vendor_url("datatables/jquery.dataTables.min.js") ?>"></script>
<script src="<?= vendor_url("datatables/dataTables.bootstrap4.min.js") ?>"></script>

<!-- Page level custom scripts -->
<script src="<?= js_url("demo/datatables-demo") ?>"></script>