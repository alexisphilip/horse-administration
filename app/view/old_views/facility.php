<!-- Page Heading -->
<h1 class="h3 mb-3 text-gray-800">Facilities</h1>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Facilities</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Family type</th>
                    <th>Level</th>
                    <th>Capacity</th>
                    <th>Price</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th>ID</th>
                    <th>Family type</th>
                    <th>Level</th>
                    <th>Capacity</th>
                    <th>Price</th>
                </tr>
                </tfoot>
                <tbody>
                <?php foreach ($facility as $key => $value) { ?>
                    <tr>
                        <td><a href="<?= base_url() ?>orm/detail/name=facility&id=<?= $value["facility_id"] ?>"><?= $value["facility_id"] ?></a></td>
                        <td><a href="<?= base_url() . "orm/detail/name=facility_family&id=" . $value["facility_family_id"] ?>"><?= $value["facility_family_id"] ?></a></td>
                        <td><?= $value["facility_level"] ?></td>
                        <td><?= $value["facility_capacity"] ?></td>
                        <td>$<?= number_format($value["facility_price"]) ?></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>


<!-- Page level plugins -->
<script src="<?= vendor_url("datatables/jquery.dataTables.min.js") ?>"></script>
<script src="<?= vendor_url("datatables/dataTables.bootstrap4.min.js") ?>"></script>

<!-- Page level custom scripts -->
<script src="<?= js_url("demo/datatables-demo") ?>"></script>