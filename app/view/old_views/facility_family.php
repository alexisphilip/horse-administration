<!-- Page Heading -->
<h1 class="h3 mb-3 text-gray-800">Facility families</h1>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Facility families</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Entity type ID</th>
                    <th>Name</th>
                    <th>Description</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th>ID</th>
                    <th>Entity type ID</th>
                    <th>Name</th>
                    <th>Description</th>
                </tr>
                </tfoot>
                <tbody>
                <?php foreach ($facility_family as $key => $value) { ?>
                    <tr>
                        <td><a href="<?= base_url() ?>orm/detail/name=facility_family&id=<?= $value["facility_family_id"] ?>"><?= $value["facility_family_id"] ?></a></td>
                        <td><a href="<?= base_url() ?>orm/detail/name=entity_type&id=<?= $value["entity_type_id"] ?>"><?= $value["entity_type_id"] ?></a></td>
                        <td><?= $value["facility_family_name"] ?></td>
                        <td><?= $value["facility_family_description"] ?></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>


<!-- Page level plugins -->
<script src="<?= vendor_url("datatables/jquery.dataTables.min.js") ?>"></script>
<script src="<?= vendor_url("datatables/dataTables.bootstrap4.min.js") ?>"></script>

<!-- Page level custom scripts -->
<script src="<?= js_url("demo/datatables-demo") ?>"></script>