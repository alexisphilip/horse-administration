<!-- Page Heading -->
<h1 class="h3 mb-3 text-gray-800">Hippodrome contests</h1>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Hippodrome contests</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Establishment ID</th>
                    <th>Contest ID</th>
                    <th>Starting time</th>
                    <th>Ending time</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th>ID</th>
                    <th>Establishment ID</th>
                    <th>Contest ID</th>
                    <th>Starting time</th>
                    <th>Ending time</th>
                </tr>
                </tfoot>
                <tbody>
                <?php foreach ($hippodrome_contest as $key => $value) { ?>
                    <tr>
                        <td><a href="<?= base_url() ?>orm/detail/name=hippodrome_contest&id=<?= $value["hippodrome_contest_id"] ?>"><?= $value["hippodrome_contest_id"] ?></a></td>
                        <td><a href="<?= base_url() . "orm/detail/name=establishment&id=" . $value["establishment_id"] ?>"><?= $value["establishment_id"] ?></a></td>
                        <td><a href="<?= base_url() . "orm/detail/name=contest&id=" . $value["contest_id"] ?>"><?= $value["contest_id"] ?></a></td>
                        <td><?= $value["hippodrome_contest_date_start"] ?></td>
                        <td><?= $value["hippodrome_contest_date_end"] ?></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>


<!-- Page level plugins -->
<script src="<?= vendor_url("datatables/jquery.dataTables.min.js") ?>"></script>
<script src="<?= vendor_url("datatables/dataTables.bootstrap4.min.js") ?>"></script>

<!-- Page level custom scripts -->
<script src="<?= js_url("demo/datatables-demo") ?>"></script>