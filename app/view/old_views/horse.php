<!-- Page Heading -->
<h1 class="h3 mb-3 text-gray-800">Horses</h1>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Horses</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Breed ID</th>
                    <th>Name</th>
                    <th>Level</th>
                    <th>Experience / 10,000</th>
                    <th>Overall state / 10</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th>ID</th>
                    <th>Breed ID</th>
                    <th>Name</th>
                    <th>Level</th>
                    <th>Experience / 10,000</th>
                    <th>Overall state / 10</th>
                </tr>
                </tfoot>
                <tbody>
                <?php foreach ($horse as $key => $value) { ?>
                    <tr>
                        <td><a href="<?= base_url() . "horse/detail/id=" . $value["horse_id"] ?>"><?= $value["horse_id"] ?></a></td>
                        <td><a href="<?= base_url() . "orm/detail/name=horse_breed&id=" . $value["horse_breed_id"] ?>"><?= $value["horse_breed_id"] ?></a></td>
                        <td><?= $value["horse_name"] ?></td>
                        <td><?= $value["horse_level"] ?></td>
                        <td><?= number_format($value["horse_experience"]) ?></td>
                        <td><?= $value["horse_overall_state"] ?></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>


<!-- Page level plugins -->
<script src="<?= vendor_url("datatables/jquery.dataTables.min.js") ?>"></script>
<script src="<?= vendor_url("datatables/dataTables.bootstrap4.min.js") ?>"></script>

<!-- Page level custom scripts -->
<script src="<?= js_url("demo/datatables-demo") ?>"></script>