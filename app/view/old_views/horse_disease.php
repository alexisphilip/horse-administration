<!-- Page Heading -->
<h1 class="h3 mb-3 text-gray-800">Horse assigned diseases</h1>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Horse assigned diseases</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Horse ID</th>
                    <th>Disease ID</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th>ID</th>
                    <th>Horse ID</th>
                    <th>Disease ID</th>
                </tr>
                </tfoot>
                <tbody>
                <?php foreach ($horse_disease as $key => $value) { ?>
                    <tr>
                        <td><a href="<?= base_url() ?>orm/detail/name=horse_disease&id=<?= $value["horse_disease_id"] ?>"><?= $value["horse_disease_id"] ?></a></td>
                        <td><a href="<?= base_url() . "horse/detail/id=" . $value["horse_id"] ?>"><?= $value["horse_id"] ?></a></td>
                        <td><a href="<?= base_url() . "orm/detail/name=disease&id=" . $value["disease_id"] ?>"><?= $value["disease_id"] ?></a></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>


<!-- Page level plugins -->
<script src="<?= vendor_url("datatables/jquery.dataTables.min.js") ?>"></script>
<script src="<?= vendor_url("datatables/dataTables.bootstrap4.min.js") ?>"></script>

<!-- Page level custom scripts -->
<script src="<?= js_url("demo/datatables-demo") ?>"></script>