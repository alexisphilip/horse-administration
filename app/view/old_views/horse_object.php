<!-- Page Heading -->
<h1 class="h3 mb-3 text-gray-800">Horse assigned objects</h1>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Horse assigned objects</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Horse ID</th>
                    <th>Object ID</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th>ID</th>
                    <th>Horse ID</th>
                    <th>Object ID</th>
                </tr>
                </tfoot>
                <tbody>
                <?php foreach ($horse_object as $key => $value) { ?>
                    <tr>
                        <td><a href="<?= base_url() ?>orm/detail/name=horse_object&id=<?= $value["horse_object_id"] ?>"><?= $value["horse_object_id"] ?></a></td>
                        <td><a href="<?= base_url() . "horse/detail/id=" . $value["horse_id"] ?>"><?= $value["horse_id"] ?></a></td>
                        <td><a href="<?= base_url() . "orm/detail/name=object&id=" . $value["object_id"] ?>"><?= $value["object_id"] ?></a></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>


<!-- Page level plugins -->
<script src="<?= vendor_url("datatables/jquery.dataTables.min.js") ?>"></script>
<script src="<?= vendor_url("datatables/dataTables.bootstrap4.min.js") ?>"></script>

<!-- Page level custom scripts -->
<script src="<?= js_url("demo/datatables-demo") ?>"></script>