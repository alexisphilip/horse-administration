<!-- Page Heading -->
<h1 class="h3 mb-3 text-gray-800">Assigned newspapers (articles list)</h1>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Assigned newspapers (articles list)</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Newspaper ID</th>
                    <th>Title</th>
                    <th>Content</th>
                    <th>Author</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th>ID</th>
                    <th>Newspaper ID</th>
                    <th>Title</th>
                    <th>Content</th>
                    <th>Author</th>
                </tr>
                </tfoot>
                <tbody>
                <?php foreach ($newspaper_article as $key => $value) { ?>
                    <tr>
                        <td><a href="<?= base_url() ?>orm/detail/name=newspaper_article&id=<?= $value["newspaper_article_id"] ?>"><?= $value["newspaper_article_id"] ?></a></td>
                        <td><a href="<?= base_url() . "newspaper/detail/id=" . $value["newspaper_id"] ?>"><?= $value["newspaper_id"] ?></a></td>
                        <td><?= $value["newspaper_article_title"] ?></td>
                        <td><?= $value["newspaper_article_content"] ?></td>
                        <td><?= $value["newspaper_article_author"] ?></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>


<!-- Page level plugins -->
<script src="<?= vendor_url("datatables/jquery.dataTables.min.js") ?>"></script>
<script src="<?= vendor_url("datatables/dataTables.bootstrap4.min.js") ?>"></script>

<!-- Page level custom scripts -->
<script src="<?= js_url("demo/datatables-demo") ?>"></script>