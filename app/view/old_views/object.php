<!-- Page Heading -->
<h1 class="h3 mb-3 text-gray-800">Objects</h1>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Objects</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Type ID</th>
                    <th>Level</th>
                    <th>Descritpion</th>
                    <th>Price</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th>ID</th>
                    <th>Type ID</th>
                    <th>Level</th>
                    <th>Descritpion</th>
                    <th>Price</th>
                </tr>
                </tfoot>
                <tbody>
                <?php foreach ($object as $key => $value) { ?>
                    <tr>
                        <td><a href="<?= base_url() ?>orm/detail/name=object&id=<?= $value["object_id"] ?>"><?= $value["object_id"] ?></a></td>
                        <td><a href="<?= base_url() . "orm/detail/name=object_type&id=" . $value["object_type_id"] ?>"><?= $value["object_type_id"] ?></a></td>
                        <td><?= $value["object_level"] ?></td>
                        <td><?= $value["object_description"] ?></td>
                        <td><?= $value["object_price"] ?></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>


<!-- Page level plugins -->
<script src="<?= vendor_url("datatables/jquery.dataTables.min.js") ?>"></script>
<script src="<?= vendor_url("datatables/dataTables.bootstrap4.min.js") ?>"></script>

<!-- Page level custom scripts -->
<script src="<?= js_url("demo/datatables-demo") ?>"></script>