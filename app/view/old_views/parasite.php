<!-- Page Heading -->
<h1 class="h3 mb-3 text-gray-800">Horse parasites</h1>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Horse parasites</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                </tr>
                </tfoot>
                <tbody>
                <?php foreach ($parasite as $key => $value) { ?>
                    <tr>
                        <td><a href="<?= base_url() ?>orm/detail/name=parasite&id=<?= $value["parasite_id"] ?>"><?= $value["parasite_id"] ?></a></td>
                        <td><?= $value["parasite_name"] ?></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>


<!-- Page level plugins -->
<script src="<?= vendor_url("datatables/jquery.dataTables.min.js") ?>"></script>
<script src="<?= vendor_url("datatables/dataTables.bootstrap4.min.js") ?>"></script>

<!-- Page level custom scripts -->
<script src="<?= js_url("demo/datatables-demo") ?>"></script>