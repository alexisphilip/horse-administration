<!-- Page Heading -->
<h1 class="h3 mb-3 text-gray-800">Purchases</h1>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Purchases</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>User ID</th>
                    <th>Establishment ID</th>
                    <th>Facility ID</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th>ID</th>
                    <th>User ID</th>
                    <th>Establishment ID</th>
                    <th>Facility ID</th>
                </tr>
                </tfoot>
                <tbody>
                <?php foreach ($purchase as $key => $value) { ?>
                    <tr>
                        <td><a href="<?= base_url() ?>orm/detail/name=sale&id=<?= $value["sale_id"] ?>"><?= $value["sale_id"] ?></a></td>
                        <td><a href="<?= base_url() . "users/detail/id=" . $value["user_id"] ?>"><?= $value["user_id"] ?></a></td>
                        <td><a href="<?= base_url() . "orm/detail/name=establishment&id=" . $value["establishment_id"] ?>"><?= $value["establishment_id"] ?></a></td>
                        <td><a href="<?= base_url() . "orm/detail/name=facility&id=" . $value["facility_id"] ?>"><?= $value["facility_id"] ?></a></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>


<!-- Page level plugins -->
<script src="<?= vendor_url("datatables/jquery.dataTables.min.js") ?>"></script>
<script src="<?= vendor_url("datatables/dataTables.bootstrap4.min.js") ?>"></script>

<!-- Page level custom scripts -->
<script src="<?= js_url("demo/datatables-demo") ?>"></script>