<!-- Page Heading -->
<h1 class="h3 mb-3 text-gray-800">User logs</h1>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">User logs</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>User ID</th>
                    <th>IP address</th>
                    <th>Connection date</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th>ID</th>
                    <th>User ID</th>
                    <th>IP address</th>
                    <th>Connection date</th>
                </tr>
                </tfoot>
                <tbody>
                <?php foreach ($user_log as $key => $value) { ?>
                    <tr>
                        <td><a href="<?= base_url() ?>orm/detail/name=user_log&id=<?= $value["user_log_id"] ?>"><?= $value["user_log_id"] ?></a></td>
                        <td><a href="<?= base_url() . "users/detail/id=" . $value["user_id"] ?>"><?= $value["user_id"] ?></a></td>
                        <td><?= $value["user_log_ip"] ?></td>
                        <td><?= $value["user_log_date_connection"] ?></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>


<!-- Page level plugins -->
<script src="<?= vendor_url("datatables/jquery.dataTables.min.js") ?>"></script>
<script src="<?= vendor_url("datatables/dataTables.bootstrap4.min.js") ?>"></script>

<!-- Page level custom scripts -->
<script src="<?= js_url("demo/datatables-demo") ?>"></script>