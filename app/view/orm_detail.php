<!-- Page Heading -->
<h1 class="h3 mb-3 text-gray-800"><?= ucfirst(orm_format_split($data["table_name"])) ?></h1>
<p>Detail for ID <strong><?= $data["table_data"][$data["table_name"] . "_id"] ?></strong></p>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <?php foreach ($data["table_data"] as $key => $value) {
                    if (substr($key, -3) == "_id" && $key !== ($data["table_name"] . "_id")) {
                        $foreign_table_name = substr($key, 0, strlen($key) - 3); ?>
                        <tr>
                            <th style="width: 50%"><?= ucfirst(orm_format_split($foreign_table_name)) ?> ID</th>
                            <?php // EXCEPTION for custom detail views
                            if (in_array($foreign_table_name, ["user", "horse", "establishment"])) {
                                if ($foreign_table_name == "user") $foreign_table_name = "users";
                                $url = $foreign_table_name . "/detail/";
                            } else {
                                $url = "orm/detail/name=" . $foreign_table_name . "&";
                            } ?>
                            <td style="width: 50%">
                                <a href="<?= base_url() . $url ?>id=<?= $value ?>"><?= $value ?></a>
                            </td>
                        </tr>
                    <?php } else if ($key == ($data["table_name"] . "_id")) { ?>
                        <tr>
                            <th style="width: 50%">ID</th>
                            <td style="width: 50%"><?= $value ?></td>
                        </tr>
                    <?php } else {
                        $field_name = orm_format_string($data["table_name"], $key); ?>
                        <tr>
                            <th style="width: 50%"><?= $field_name ?></th>
                            <?php if ($field_name == "Price") { ?>
                                <td style="width: 50%">$<?= number_format($value) ?></td>
                            <?php } else { ?>
                                <td style="width: 50%"><?= $value ?></td>
                            <?php } ?>
                        </tr>
                    <?php }
                } ?>
            </table>
        </div>
    </div>
</div>
