<!-- Page Heading -->
<h1 class="h3 mb-3 text-gray-800"><?= ucfirst(orm_format_split($table_name)) ?></h1>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                <tr>
                    <?php foreach ($table_columns as $field) {
                        if (substr($field["Field"], -3) == "_id" && $field["Field"] !== ($table_name . "_id")) {
                            $foreign_table_name = substr($field["Field"], 0, strlen($field["Field"]) - 3); ?>
                            <th><?= ucfirst(orm_format_split($foreign_table_name)) ?> ID</th>
                        <?php } else if ($field["Field"] == ($table_name . "_id")) { ?>
                            <th>ID</th>
                        <?php } else {
                            $field_name = orm_format_string($table_name, $field["Field"]); ?>
                            <th><?= $field_name ?></th>
                        <?php } ?>
                    <?php } ?>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($table_data as $value) { ?>
                    <tr>
                        <?php foreach ($table_columns as $field) {
                            $key = $field["Field"];
                            if (substr($key, -3) == "_id" && $key !== ($table_name . "_id")) {
                                $foreign_table_name = substr($key, 0, strlen($key) - 3); ?>
                                <?php // EXCEPTION for custom detail views
                                if (in_array($foreign_table_name, ["user", "horse", "establishment", "newspaper"])) {
                                    if ($foreign_table_name == "user") $foreign_table_name = "users";
                                    $url = $foreign_table_name . "/detail/";
                                } else {
                                    $url = "orm/detail/name=" . $foreign_table_name . "&";
                                } ?>
                                <td>
                                    <a href="<?= base_url() . $url ?>id=<?= $value[$key] ?>"><?= $value[$key] ?></a>
                                </td>
                            <?php } else if ($key == ($table_name . "_id")) {
                                // EXCEPTION for custom detail views
                                if (in_array($table_name, ["user", "horse", "establishment", "newspaper"])) {
                                    if ($table_name == "user") $table_name = "users";
                                    $url = $table_name . "/detail/";
                                } else {
                                    $url = "orm/detail/name=" . $table_name . "&";
                                } ?>
                                <td>
                                    <a href="<?= base_url() . $url ?>id=<?= $value[$key] ?>"><?= $value[$key] ?></a>
                                </td>
                            <?php } else {
                                $field_name = orm_format_string($table_name, $key); ?>
                                <?php if ($field_name == "Price") { ?>
                                    <td>$<?= number_format($value[$key]) ?></td>
                                <?php } else { ?>
                                    <td><?= $value[$key] ?></td>
                                <?php } ?>
                            <?php } ?>
                        <?php } ?>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- Page level plugins -->
<script src="<?= vendor_url("datatables/jquery.dataTables.min.js") ?>"></script>
<script src="<?= vendor_url("datatables/dataTables.bootstrap4.min.js") ?>"></script>

<!-- Page level custom scripts -->
<script src="<?= js_url("demo/datatables-demo") ?>"></script>
