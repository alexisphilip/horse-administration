<?php //p($data); ?>
<div class="container-fluid">
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-info">Update
                <strong> <?= ucfirst(orm_format_split($data["table_name"])) ?> </strong></h6>
        </div>
        <div class="card-body px-5 py-4">
            <form action="">
                <?php foreach ($data["table_data"] as $key => $value) {
                    if ($value["option"]) { ?>
                        <!-- Select-->
                        <div class="row">
                            <label class="text-primary mr-2"
                                   for="<?= $value['name'] ?>"><?= ucfirst(orm_format_split($value['name'])) ?></label>
                            <select type="text" name="<?= $value['name'] ?>">
                                <?php foreach ($value["option"] as $options) {
                                    $selected = ($options == $value["value"] ? "selected" : "") ?>
                                    <option value="<?= $options ?>" <?= $selected ?>><?= $options ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    <?php } else if ($value["option"] == false && $value["range"] == false) { ?>
                        <!-- Texarea-->
                        <div class="row">
                            <label class="text-primary mr-2"
                                   for="<?= $value['name'] ?>"><?= ucfirst(orm_format_split($value['name'])) ?></label>
                            <textarea class="w-100" name="<?= $value['name'] ?>" id="<?= $value['name'] ?>"
                                      rows="3"><?= $value["value"] ?></textarea>
                        </div>
                    <?php } else {
                        if ($value["type"] == "int") { ?>
                            <!-- Type number-->
                            <div class="row">
                                <label class="text-primary mr-2"
                                       for="<?= $value['name'] ?>"><?= ucfirst(orm_format_split($value['name'])) ?></label>
                                <input class="w-100" type="number" value="<?= $value["value"] ?>"
                                       min="<?= $value["range"][0] ?>"
                                       max="<?= $value["range"][1] ?>"
                                       name="<?= $value['name'] ?>">
                            </div>
                        <?php } else { ?>
                            <!-- Type text-->
                            <div class="row">
                                <label class="text-primary mr-2"
                                       for="<?= $value['name'] ?>"><?= ucfirst(orm_format_split($value['name'])) ?></label>
                                <input class="w-100" type="text" minlength="<?= $value["range"][0] ?>"
                                       maxlength="<?= $value["range"][1] ?>"
                                       name="<?= $value['name'] ?>" value="<?= $value["value"] ?>">
                            </div>
                        <?php }
                    }
                } ?>
                <div class="row mt-4 d-flex justify-content-center">
                    <button type="submit" class="btn btn-success btn-icon-split">
                    <span class="icon text-white-50">
                      <i class="fas fa-check"></i>
                    </span>
                        <span class="text">Submit</span>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
