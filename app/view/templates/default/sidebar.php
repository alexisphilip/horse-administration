<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?= base_url() . "dashboard" ?>">
        <span class="sidebar-brand-text mx-3">HORSE ADMINISTRATION</span>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item active">
        <a class="nav-link" href="<?= base_url() . "dashboard" ?>">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span>
        </a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        Interface
    </div>

    <!-- Nav Item -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUsers"
           aria-expanded="true" aria-controls="collapseUsers">
            <i class="fas fa-fw fa-user"></i>
            <span>Users</span>
        </a>
        <div id="collapseUsers" class="collapse" aria-labelledby="headingUsers"
             data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Tables</h6>
                <a class="collapse-item" href="<?= base_url() . "orm/list/name=users" ?>">All users</a>
                <a class="collapse-item" href="<?= base_url() . "orm/list/name=membership" ?>">Memberships</a>
                <a class="collapse-item" href="<?= base_url() . "orm/list/name=user_log" ?>">Log history</a>
            </div>
        </div>
    </li>

    <!-- Nav Item -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseEstablishments"
           aria-expanded="true" aria-controls="collapseEstablishments">
            <i class="fas fa-fw fa-industry"></i>
            <span>Establishments</span>
        </a>
        <div id="collapseEstablishments" class="collapse" aria-labelledby="headingEstablishments"
             data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Tables</h6>
                <a class="collapse-item" href="<?= base_url() . "orm/list/name=establishment" ?>">All establishments</a>
                <a class="collapse-item" href="<?= base_url() . "orm/list/name=establishment_facility" ?>">Assigned facilities</a>
                <a class="collapse-item" href="<?= base_url() . "orm/list/name=establishment_type" ?>">Types</a>
            </div>
        </div>
    </li>

    <!-- Nav Item -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseFacilities"
           aria-expanded="true" aria-controls="collapseFacilities">
            <i class="fas fa-fw fa-home"></i>
            <span>Facilities</span>
        </a>
        <div id="collapseFacilities" class="collapse" aria-labelledby="headingFacilities"
             data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Tables</h6>
                <a class="collapse-item" href="<?= base_url() . "orm/list/name=facility" ?>">All facilities</a>
                <a class="collapse-item" href="<?= base_url() . "orm/list/name=facility_item" ?>">Assigned items</a>
                <a class="collapse-item" href="<?= base_url() . "orm/list/name=facility_family" ?>">Families</a>
                <a class="collapse-item" href="<?= base_url() . "orm/list/name=entity_type" ?>">Types</a>
            </div>
        </div>
    </li>

    <!-- Nav Item -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseCommerce"
           aria-expanded="true" aria-controls="collapseCommerce">
            <i class="fas fa-fw fa-dollar-sign"></i>
            <span>Commerce</span>
        </a>
        <div id="collapseCommerce" class="collapse" aria-labelledby="headingCommerce"
             data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Tables</h6>
                <a class="collapse-item" href="<?= base_url() . "orm/list/name=sale" ?>">Sales</a>
                <a class="collapse-item" href="<?= base_url() . "orm/list/name=purchase" ?>">Purchases</a>
            </div>
        </div>
    </li>

    <!-- Nav Item -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseHorses"
           aria-expanded="true" aria-controls="collapseHorses">
            <i class="fas fa-fw fa-horse-head"></i>
            <span>Horses</span>
        </a>
        <div id="collapseHorses" class="collapse" aria-labelledby="headingHorses"
             data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Tables</h6>
                <a class="collapse-item" href="<?= base_url() . "orm/list/name=horse" ?>">All horses</a>
                <a class="collapse-item" href="<?= base_url() . "orm/list/name=horse_breed" ?>">Breeds</a>
                <a class="collapse-item" href="<?= base_url() . "orm/list/name=horse_object" ?>">Assigned objects</a>
            </div>
        </div>
    </li>

    <!-- Horse -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseMedical"
           aria-expanded="true" aria-controls="collapseMedical">
            <i class="fas fa-fw fa-ambulance"></i>
            <span>Medical data</span>
        </a>
        <div id="collapseMedical" class="collapse" aria-labelledby="headingMedical"
             data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Medical information</h6>
                <a class="collapse-item" href="<?= base_url() . "orm/list/name=horse_disease" ?>">Assigned diseases</a>
                <a class="collapse-item" href="<?= base_url() . "orm/list/name=horse_parasite" ?>">Assigned parasites</a>
                <a class="collapse-item" href="<?= base_url() . "orm/list/name=horse_injury" ?>">Assigned injuries</a>
                <h6 class="collapse-header">Tables</h6>
                <a class="collapse-item" href="<?= base_url() . "orm/list/name=disease" ?>">Diseases</a>
                <a class="collapse-item" href="<?= base_url() . "orm/list/name=parasite" ?>">Parasites</a>
                <a class="collapse-item" href="<?= base_url() . "orm/list/name=injury" ?>">Injuries</a>
            </div>
        </div>
    </li>

    <!-- Nav Item -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseObjects"
           aria-expanded="true" aria-controls="collapseObjects">
            <i class="fas fa-fw fa-object-group"></i>
            <span>Objects</span>
        </a>
        <div id="collapseObjects" class="collapse" aria-labelledby="headingObjects"
             data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Tables</h6>
                <a class="collapse-item" href="<?= base_url() . "orm/list/name=object" ?>">All objects</a>
                <a class="collapse-item" href="<?= base_url() . "orm/list/name=object_type" ?>">Types</a>
                <a class="collapse-item" href="<?= base_url() . "orm/list/name=object_family" ?>">Families</a>
            </div>
        </div>
    </li>

    <!-- Nav Item -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseContests"
           aria-expanded="true" aria-controls="collapseContests">
            <i class="fas fa-fw fa-medal"></i>
            <span>Contests</span>
        </a>
        <div id="collapseContests" class="collapse" aria-labelledby="headingContests"
             data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Tables</h6>
                <a class="collapse-item" href="<?= base_url() . "orm/list/name=hippodrome_contest" ?>">All contests<br>(assigned contests)</a>
                <a class="collapse-item" href="<?= base_url() . "orm/list/name=contest" ?>">Contests names</a>
                <a class="collapse-item" href="<?= base_url() . "orm/list/name=contest_prize" ?>">Prizes</a>
            </div>
        </div>
    </li>

    <!-- Nav Item -->
    <li class="nav-item">
        <a class="nav-link" href="<?= base_url() ?>orm/list/name=membership">
            <i class="fas fa-fw fa-table"></i>
            <span>Membership</span>
        </a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        Automation
    </div>

    <!-- Nav Item -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseAutomaticTasks"
           aria-expanded="true" aria-controls="collapseAutomaticTasks">
            <i class="fas fa-fw fa-car"></i>
            <span>Automatic tasks</span>
        </a>
        <div id="collapseAutomaticTasks" class="collapse" aria-labelledby="headingAutomaticTasks"
             data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Tables</h6>
                <a class="collapse-item" href="<?= base_url() . "orm/list/name=automatic_task" ?>">Current automations</a>
                <a class="collapse-item" href="<?= base_url() . "orm/list/name=task_action" ?>">Types</a>
            </div>
        </div>
    </li>

    <!-- Nav Item -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseNewspaper"
           aria-expanded="true" aria-controls="collapseNewspaper">
            <i class="fas fa-fw fa-pencil-alt"></i>
            <span>Newspaper</span>
        </a>
        <div id="collapseNewspaper" class="collapse" aria-labelledby="headingNewspaper"
             data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Articles & newspaper</h6>
                <a class="collapse-item" href="<?= base_url() . "orm/list/name=newspaper" ?>">Newspapers</a>
                <a class="collapse-item" href="<?= base_url() . "orm/list/name=newspaper_article" ?>">Assigned newspapers<br>(articles list)</a>
                <h6 class="collapse-header">Advertisements</h6>
                <a class="collapse-item" href="<?= base_url() . "orm/list/name=newspaper_advertisement" ?>">Assigned advertisements</a>
                <a class="collapse-item" href="<?= base_url() . "orm/list/name=advertisement" ?>">All advertisements</a>
            </div>
        </div>
    </li>


    <!-- Divider -->
<!--    <hr class="sidebar-divider">-->
<!---->
    <!-- Heading -->
<!--    <div class="sidebar-heading">-->
<!--        Maintenance-->
<!--    </div>-->

    <!-- Nav Item -->
<!--    <li class="nav-item">-->
<!--        <a class="nav-link" href="--><?//= base_url() . "clients.php" ?><!--">-->
<!--            <i class="fas fa-fw fa-table"></i>-->
<!--            <span>Database copy</span>-->
<!--        </a>-->
<!--    </li>-->


    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>
<!-- End of Sidebar -->