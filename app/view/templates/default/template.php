<!DOCTYPE html>
<html lang="<?= $language_code ?>">
<head>
    <!-- Title -->
    <title><?= $page_title ?></title>

    <!-- File encoding -->
    <meta charset="UTF-8">

    <!-- Viewport -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Meta elements -->
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">

    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="120x120" href="<?= img_url("favicon/apple-touch-icon.png") ?>">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= img_url("favicon/favicon-32x32.png") ?>">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= img_url("favicon/favicon-16x16.png") ?>">
    <link rel="manifest" href="<?= img_url("favicon/site.webmanifest/") ?>">
    <link rel="mask-icon" href="<?= img_url("favicon/safari-pinned-tab.svg") ?>" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">


    <!-- Custom fonts for this template -->
    <link href="<?= vendor_url("fontawesome-free/css/all.min.css") ?>" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
          rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?= css_url("sb-admin-2.min") ?>" rel="stylesheet">

    <!-- Bootstrap core JavaScript -->
    <script src="<?= vendor_url("jquery/jquery.min.js") ?>"></script>
    <script src="<?= vendor_url("bootstrap/js/bootstrap.bundle.min.js") ?>"></script>

    <!-- Core plugin JavaScript -->
    <script src="<?= vendor_url("jquery-easing/jquery.easing.min.js") ?>"></script>

    <!-- Custom scripts for all pages -->
    <script src="<?= js_url("sb-admin-2.min") ?>"></script>

    <!-- Custom style for table pages -->
    <link href="<?= vendor_url("datatables/dataTables.bootstrap4.min.css") ?>" rel="stylesheet">

    <!-- CSS files -->
    <?php foreach ($css_urls as $css_url) { ?>
        <link href="<?= css_url($css_url) ?>" rel="stylesheet">
    <?php } ?>
</head>

<body id="page-top">

<!-- Page Wrapper -->
<div id="wrapper">

    <?php include "sidebar.php" ?>

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

        <!-- Main Content -->
        <div id="content">

            <?php include "topbar.php" ?>

            <!-- Begin Page Content -->
            <div class="container-fluid">

                <?php include $view_path ?>

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- End of Main Content -->

        <!-- Footer -->
        <footer class="sticky-footer bg-white">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>HORSE ADMINISTRATION - 2020</span>
                </div>
            </div>
        </footer>
        <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- JS files -->
<?php foreach ($js_urls as $js_url) { ?>
    <script src="<?= js_url($js_url) ?>"></script>
<?php } ?>

</body>
</html>
