<!-- Page Heading -->
<h1 class="h3 mb-3 text-gray-800">Users</h1>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Users</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Account ID</th>
                    <th>Gender</th>
                    <th>F-name</th>
                    <th>L-name</th>
                    <th>Username</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Birth date</th>
                    <th>Balance</th>
                    <th>Creation date</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th>ID</th>
                    <th>Account ID</th>
                    <th>Gender</th>
                    <th>F-name</th>
                    <th>L-name</th>
                    <th>Username</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Birth date</th>
                    <th>Balance</th>
                    <th>Creation date</th>
                </tr>
                </tfoot>
                <tbody>
                <?php foreach ($table_data as $key => $value) { ?>
                    <tr>
                        <td><a href="<?= base_url() . "users/detail/id=" . $value["user_id"] ?>"><?= $value["user_id"] ?></a></td>
                        <td><?= $value["user_account_id"] ?></td>
                        <td><?php echo ($value["user_gender"] ? "M" : "F") ?></td>
                        <td><?= $value["user_f_name"] ?></td>
                        <td><?= $value["user_l_name"] ?></td>
                        <td><?= $value["user_username"] ?></td>
                        <td><?= $value["user_email"] ?></td>
                        <td><?= $value["user_phone"] ?></td>
                        <td><?= $value["user_birth_date"] ?></td>
                        <td>$<?= number_format($value["user_balance"]) ?></td>
                        <td><?= $value["user_creation_date"] ?></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>


<!-- Page level plugins -->
<script src="<?= vendor_url("datatables/jquery.dataTables.min.js") ?>"></script>
<script src="<?= vendor_url("datatables/dataTables.bootstrap4.min.js") ?>"></script>

<!-- Page level custom scripts -->
<script src="<?= js_url("demo/datatables-demo") ?>"></script>