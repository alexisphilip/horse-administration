<?php
if ($user_gender == 2) {
    $gender = "Female";
} else {
    $gender = "Male";
}
?>


<div class="container-fluid">

    <div class="row">

        <div class="col-xl-8 col-lg-7">

            <div class="card shadow pt-4 px-4 mb-4">
                <h1><?= $user_username ?></h1>
                <div class="d-flex justify-content-between align-items-center">
                    <p><?= $user_f_name ?> <?= $user_l_name ?></p>
                    <?php $split = explode(" ", $user_creation_date); ?>
                    <p>Joined on <strong><?= $split[0] ?></strong> at <strong><?= $split[1] ?></strong></p>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6 col-md-5">
                    <div class="card border-left-info shadow mb-4">
                        <div class="card-body pb-2">
                            <div class="d-flex justify-content-between align-items-center">
                                <h3 class="text-lg font-weight-bold text-info">Total horses</h3>
                                <h3><strong><?= number_format($total_horses) ?></strong></h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-5">
                    <div class="card border-left-danger shadow mb-4">
                        <div class="card-body pb-2">
                            <div class="d-flex justify-content-between align-items-center">
                                <h3 class="text-lg font-weight-bold text-danger">Total facilities</h3>
                                <h3><strong><?= number_format($total_facilities) ?></strong></h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card shadow pt-4 px-4 mb-4">
                <h4>Description</h4>
                <div class="d-flex justify-content-between align-items-center">
                    <p class="text-md">« <?= $user_description ?> »</p>
                </div>
            </div>

        </div>

        <div class="col-xl-4 col-lg-5">

            <div class="card border-info shadow mb-4" style="border-width: 3px">
                <!-- Card Header - Accordion -->
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-info">Contact information</h6>
                </div>
                <!-- Card Content - Collapse -->
                <div class="card-body pb-0">
                    <table style="width: 100%;" class="table">
                        <tr>
                            <th style="border-top: none;">First Name</th>
                            <td style="border-top: none;" class="text-right"><?= $user_f_name ?></td>
                        </tr>
                        <tr>
                            <th>Last Name</th>
                            <td class="text-right"><?= $user_l_name ?></td>
                        </tr>
                        <tr>
                            <th>Email</th>
                            <td class="text-right" style="overflow: hidden"><?= $user_email ?></td>
                        </tr>
                        <tr>
                            <th>Phone</th>
                            <td class="text-right"><?= $user_phone ?></td>
                        </tr>
                        <tr>
                            <th>Address</th>
                            <td class="text-right"><?= $user_address ?></td>
                        </tr>
                        <tr>
                            <th>Website</th>
                            <td class="text-right"><a href="<?= $website ?>"><?= $website ?></a></td>
                        </tr>
                    </table>
                </div>
            </div>

        </div>

    </div>

    <!-- Tables -->
    <div class="row">

        <div class="col">
            <div class="card border-left-warning shadow mb-4" style="border-width: 3px">
                <!-- Card Header - Accordion -->
                <a href="#collapseCardEstablishment" class="d-block card-header py-3" data-toggle="collapse"
                   role="button" aria-expanded="true" aria-controls="collapseCardEstablishment">
                    <h6 class="m-0 font-weight-bold text-primary">5 best establishments (most expensive)</h6>
                </a>
                <!-- Card Content - Collapse -->
                <div class="collapse show" id="collapseCardEstablishment">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <tr>
                                    <th style="border-top: none;">ID</th>
                                    <th style="border-top: none;">Type</th>
                                    <th style="border-top: none;">Price</th>
                                    <th style="border-top: none;">Total facilities</th>
                                </tr>
                                <?php foreach ($user_establishments as $value) { ?>
                                    <tr>
                                        <td>
                                            <a href="<?= base_url() . "establishment/detail/id=" . $value["establishment_id"] ?>"><?= $value["establishment_id"] ?></a>
                                        </td>
                                        <td><?= $value["establishment_type_name"] ?></td>
                                        <td>$<?= number_format($value["establishment_price"]) ?></td>
                                        <td>
                                            <a href="<?= base_url() . "orm/list/name=establishment_facility&where=establishment_id," . $value["establishment_id"] ?>"><?= $value["total_facilities"] ?></a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col">
            <div class="card border-left-primary shadow mb-4">
                <!-- Card Header - Accordion -->
                <a href="#collapseCardLog" class="d-block card-header py-3" data-toggle="collapse"
                   role="button" aria-expanded="true" aria-controls="collapseCardLog">
                    <h6 class="m-0 font-weight-bold text-primary">5 last logs</h6>
                </a>
                <!-- Card Content - Collapse -->
                <div class="collapse show" id="collapseCardLog">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <tr>
                                    <th style="border-top: none;">ID</th>
                                    <th style="border-top: none;">Date</th>
                                    <th style="border-top: none;">IP</th>
                                </tr>
                                <?php foreach ($user_log as $value) { ?>
                                    <tr>
                                        <td>
                                            <a href="<?= base_url() . "orm/detail/name=user_log&id=" . $value['user_log_id'] ?>"><?= $value['user_log_id'] ?></a>
                                        </td>
                                        <td><?= DateConverter::convert($value["user_log_date_connection"]) ?></td>
                                        <td><?= $value['user_log_ip'] ?> </td>
                                    </tr>
                                <?php } ?>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col">
            <div class="card border-left-info shadow mb-4" style="border-width: 3px">
                <!-- Card Header - Accordion -->
                <a href="#collapseCardHorses" class="d-block card-header py-3" data-toggle="collapse"
                   role="button" aria-expanded="true" aria-controls="collapseCardExample">
                    <h6 class="m-0 font-weight-bold text-primary">5 best horses (most experience)</h6>
                </a>
                <!-- Card Content - Collapse -->
                <div class="collapse show" id="collapseCardHorses">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <tr>
                                    <th style="border-top: none;">ID</th>
                                    <th style="border-top: none;">Name</th>
                                    <th style="border-top: none;">Experience</th>
                                </tr>
                                <?php foreach ($user_horses as $value) { ?>
                                    <tr>
                                        <td>
                                            <a href="<?= base_url() . "horse/detail/id=" . $value["horse_id"] ?>"><?= $value["horse_id"] ?></a>
                                        </td>
                                        <td><?= $value["horse_name"] ?></td>
                                        <td><?= number_format($value["horse_experience"]) ?> xp.</td>
                                    </tr>
                                <?php } ?>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>

<!-- Page level plugins -->
<script src="<?= vendor_url("chart.js/Chart.min.js") ?>"></script>