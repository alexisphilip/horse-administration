# Documentation de rendu pour Horse Administration.

URL : https://horse-administration.alexisphilip.fr/docs/

Ceci est le compte rendu en ligne du projet SI BD [Horse Administration](https://gitlab.com/alexis-philip/horse-administration).

# Auteurs

## Rédaction

- Alexis Philip @alexis-philip
- Laura Samaolo @Slaura
- Nicolas Wasner @ElWesnar1

## Mise en page et création de la doc. docsify

- Alexis Philip

# Ressources

Documentation réalisée avec *docsify* : https://docsify.js.org/#/