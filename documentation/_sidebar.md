- Présentation
    - [Introduction](introduction)
    - [Historique du projet](historique)
    - [Cloture du travail](cloture-du-travail)

- Étapes de développement
    - [Architecture BDD](architecture-bdd)
    - [Utilisateurs MySQL](utilisateurs-mysql)
    - [Génération de données](generation-donnees)
    - [Interface d'administration](interface-administration)
    - [Maintenance et surveillance](maintenance-surveillance)
    - [Sauvegarde, réplication et sécurité](sauvegarde-replication-sécurite)
    
- Technique
    - [Choix techniques](choix-techniques)
    - [Framework PHP](framework-php)
    
- Autres
    - [Liens et ressources](liens-et-ressources)
