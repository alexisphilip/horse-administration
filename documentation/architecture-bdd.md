# Architecture BDD

![](img/schema.jpg)

## Syntaxe

Nous nous sommes mis d'accord sur un syntaxe commune pour les noms de tables et noms de champs de la base de données.

### Tables

Les tables sont nommées en **minuscules**, **au singulier**, **noms séparés par des traits d'union**.

?> Les tables d'associations sont nommés par les noms des deux tables liant les tuples entre eux. 

Exemples :
```text
hippodrome
contest
hippodrome_contest (lie les tuples de contest aux tuples de hippodrome)
```

?> Exception : la table `users` ne peut pas être au singulier car c'est un mot-clé déjà réservé par MySQL qui
à nommé sa table d'utilisateurs `user`.

### Champs

Les champs sont nommés en **minuscules**, **au singulier**, **noms séparés par des traits d'union** et **portent le nom 
de la table au début**.

Table d'association `hippodrome` :

|     | hippodrome_contest            |
|-----|-------------------------------|
| PK  | hippodrome_contest_id         |
| FK1 | contest_id                    |
| FK2 | establishment_id              |
|     | hippodrome_contest_date_start |
|     | hippodrome_contest_date_end   |

Table associée 1 `establishment` :

|     | establishment          |
|-----|------------------------|
| PK  | establishment_id       |
| FK1 | user_id                |
|     | establishment_capacity |
|     | ...                    |

Table associée 1 `contest` :

|    | contest      |
|----|--------------|
| PK | contest_id   |
|    | contest_name |
|    | ...          |

?> Exception : clés étrangères ne portent pas le nom de la table.

## /!\ `item` !== `object`

!> Dans cette base de données, nous avons décidé de changer légèrement le nommage donné dans la consigne.

Nous avons changé le nom d'`item` à `object` car un `facility` peut contenir des `horse` ou des `object`.  

Voici la règle :
- `item` === `horse`
- `item` === `object`

Les `items` sont alors une catégorie qui peuvent être soit un `horse`, soit un `object`. 

Cette différence est réféncée dans la table `entity_type`. 

## ORM

Respecter une syntaxe stricte nous a permis de développer un [ORM semi-automatique (voir section)](https://horse-administration.alexisphilip.fr/docs/#/interface-administration?id=orm).

@auteur_page : Alexis Philip