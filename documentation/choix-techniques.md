# Choix techniques

## Versionnement

Nous avons utilisé [GitLab](https://gitlab.com/alexis-philip/horse-administration) pour versionner le projet.

Au préalable, Alexis à travaillé sur le framework Birdy sur [Birdy](https://birdy.alexisphilip.fr). En suite, nous 
avons commencé à développer l'interface d'administration en versionnant le projet sur Gitlab.

## VPS (Virtual private server)

Nous avons mis en place un VPS avec le système d'exploitation Linux, distribution Debian minimale (voir capture
d'écran ci-dessous).

![](img/debian-version.png)

Nous avons :
- installé LAMP ;
- mis en place des utilistateurs UNIX (pour les accès SSH et SFTP) ;
- mis en place un contrat SSL officiel (non auto-signé) ;
- configuré un DNS (pour le domaine alexisphilip.fr).

Nous avons choisis de travailler en commun sur un VPS pour centraliser nos données, et travailler sur le même système.

Cela nous a permis de d'éviter les problèmes liés aux différents systèmes d'exploitation, ou versions d'applications
(apache2, phpmyadmin, etc.). 

## Myisamchk

Nous avons décidé de réaliser des scripts en myisamchk car il est fonctionnellement équivalent aux commandes 
`[CHECK | REPAIR | ANALYZE]`. 

Il nous permet de cibler les analyser qui nous parraîssait importantes, par exemple `[SILENT]` qui n’affiche que les 
erreurs ou encore `[FAST]` qui vérifie seulement les tables qui n’ont pas été correctement fermées. 

L’option `[MEDIUM-CHECK]` est une alternative plus longue mais qui permet de vérifier toutes les tables (pas seulement 
celles qui ont été sélectionnées).

Sa principale différence est que ce n’est pas un client, mais un utilitaire qui s’attaque directement aux fichiers des 
tables MyISAM.

@auteur_page : Alexis Philip, Laura Sampaolo