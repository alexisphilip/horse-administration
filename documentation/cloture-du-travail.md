# Cloture du projet

## Bilan technique

Ce projet s’est avéré très instructif pour chacun d’entre nous. Nous avons pu découvrir des problèmes qui
s’appliquent à de grandes entreprises et à de grands groupes qui manipulent de tels volumes de données.
Avec toute la logique et les architectures à monter derrière pour manipuler des volumes de données aussi conséquent.

Ce projet nous a aussi permis de prendre conscience de ce qu’il est possible de réaliser avec une base de
données architecturalement correcte, et les opérations de sauvegarde, de réparation et d'optimisation.

Le plus complexe a été de concevoir la base de données. Il était important de partir sur une syntaxe commune pour que 
la base soit compatible avec l'ORM semi-automatique de Birdy.

C’est pourquoi, nous avons passé beaucoup de temps à concevoir la base de données de façon minutieuse pour avoir le plus
de liberté par la suite tout en optimisant au maximum en faisant quelques concessions par moment.

Nous avions pensé développer l'interface d'administration avec les frameworks Symfony ou Laravel pour l’utilisation de
migrations, factories et seeders mais nous avions préféré utiliser un framework plus simple d'approche, qui possède 
également un ORM et une gestion des utilisateurs SQL.

Nous avons opté pour une autre solution [Birdy](https://birdy.alexisphilip.fr) un framework réalisé par Alexis Philip
dans le cadre d’un projet PHP plus tôt dans l’année. Grâce à celui-ci nous avons pu gagner du temps de développement
grâce à l'ORM semi-automatique.

## Problèmes rencontrés

La réalisation de ce projet a entraîné plusieurs problèmes

### Problèmes résolus

- Installation complète de LAMP sur une Debian minimale sur un VPS (hébergé chez Hostinger) ;
- Gestion du DNS, des users UNIX (pour SSH et SFTP) et users MySQL ;
- Développement d'un ORM fonctionnel pour list et detail des tuples de la BDD ;
- Schéma relationnel de la base pas optimisé pour l’utilisation dans notre cas précis.

### Problème non résolus

- Système d'update des tuples de l'ORM.


## Écarts avec les prévisions

- Pas d'espace de connexion pour les multiples utilisateurs de l'application (MySQL users) ;
- ORM ne supporte pas encore l'update, l'insert et le delete complet ;
- Pas de réplication active ;
- Pas de possibilité d’accéder à la journalisation ou de demander un diagnostic de la base depuis l'interface admin ;
- Pas de sécurité avancé mise en place.

## Mesures d’améliorations

Le projet peut être soumis à plusieurs améliorations.

### Améliorations technologiques

- Mise en place d’une réplication complète de l’application avec système Maître/Esclave.
- Mise en place de vue détaillées pour toutes les données de la Base (Facilities,Contest,..).
- Implémentation totale d’une maintenance des données disponible depuis l’interface d’administration.
- CRUD pour chaque utilisateur accédant à des vues dynamiquement créé en fonction des droits de l’utilisateur connecté.
- Ajout de fonctionnalités Insert dans l’ORM avec une "intelligence" partielle (vérification des clés étrangères avec relation autonome)
- Ajout de fonctionnalités permettant d’accéder aux logs et à la journalisation depuis l’interface admin à tout moment.

### Améliorations sécuritaires

- Mise en place d’une sensibilité générale (avec une matrice de sensibilisation)
- Une restriction réseau (limiter les connexions au port)
- Mise en place d”un chrooting (racine virtuelle dans un dossier)
- Plusieurs noyaux virtuels en espace utilisateur
- Une émulation d’une machine virtuelle complète
- Mise en place de la paravirtualisation (noyau en espace utilisateur + machine virtuelle)


## Conclusion, en clair, ce que nous avons appris

Ce projet nous a appris à concevoir un système d'information et de base de données, plus précisément :

- à conçevoir une base de donnée relationelle en suivant les propriétés ACID ;
- à comprendre l'importance des utilisateurs MySQL ;
- à développer un ORM pour facilité le développement des vues sur l'interface d'administration ;
- à administrer un serveur Web : installation de LAMP, gestion des utilisateurs UNIX (SSH, SFTP)
 
Nous avons renforcés une majeur partie des connaissances acquises lors des cours de cette année, le travail d’équipe et surtout de comprendre combien la communication à de l’importance. 

Avoir réalisé ce projet est pour nous synonyme de fierté et de soulagement car il souligne parfaitement une année passée au sein d’une école, d’un groupe. 

@auteur_page : Nicolas Wasner, Laura Sampaolo, Alexis Philip