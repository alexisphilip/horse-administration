# Framework PHP

## Présentation 

Birdy est le framework PHP utilisé pour ce projet. Il a été conçu et développé au préalable pour un autre projet, et 
a été amélioré pour subvenir au besoins de ce projet (développement d'un ORM semi-automatique).

> [Documentation complète de Birdy](https://birdy.alexisphilip.fr/#/)

## Fonctionnalités

Birdy propose de multiples fonctionnalités natives.

- Architecture MVC ;
- Multiples connexions de bases de données ;
- Auto chargement (groupes de CSS, JS, etc) ;
- Patrons de vues ;
- Multi-langues.

## Architecture MVC

Créer une interface d'administration web nécessite une certaine rigueur dans la conception de l'architecture PHP.

L'architecture de ce projet suit le **patron de conception M.V.C. (orienté web)**. Ce patron de conception est très utilisé dans le web et 
nous avons trouvé qu'il était nécessaire de le mettre en place pour l'application.

Suite à une requête HTTP, le routeur (instancié depuis **index.php**) instancie un contrôleur. Celui-ci instancie des 
modèles qui traitent des données, puis inclus une ou plusieurs vues grâce aux patrons.

> En apprendre plus sur l'[architecture de Birdy](https://birdy.alexisphilip.fr/#/tutorial/architecture)

## Connexions bases de données multiples

Birdy propose de manière native la connexion à multiples bases de données.

Dans ce projet d'administration, cela nous a permis de déclarer tous les utilistateurs MySQL, et d'instancer une connexion avec celui-ci
en fonction de la table requêtée par l'[ORM (voir section)](https://horse-administration.alexisphilip.fr/docs/#/interface-administration?id=orm).

Explication :

Il faut [définir les informations de connexion](https://birdy.alexisphilip.fr/#/tutorial/fetching-data?id=from-the-model) 
aux bases de données dans le fichier de configuration, puis 
[instancier la / les connexions](https://birdy.alexisphilip.fr/#/tutorial/fetching-data?id=from-the-model) depuis le modèle 
dans le constructeur.

## Auto-chargement

Birdy propose l'auto chargement des :

- `assets` (fichiers CSS et JS) ;
- `helpers` (scripts PHP utilisables dans la vue) ;
- `traduction` (fichiers de traduction).

Les scripts peuvent être [chargés manuellement](https://birdy.alexisphilip.fr/#/tutorial/assets-loading?id=manual-loading) 
depuis le contrôlleur ou [automatiquement](https://birdy.alexisphilip.fr/#/tutorial/assets-loading?id=auto-loading) depuis 
le fichier de configuration "**autoloading**".

Il est également possible de 
[définir des groupes](https://birdy.alexisphilip.fr/#/tutorial/assets-loading?id=auto-load-groups-of-files) pour charger ces 
fichiers en groupe avec qu'un seul appel.

> En apprendre plus sur l'[auto-chargement](https://birdy.alexisphilip.fr/#/tutorial/assets-loading).

## Patrons de vues

Il est possible de définir plusieurs patrons de vues, appelables depuis le 
[contrôleur](https://birdy.alexisphilip.fr/#/guide/controller?id=view).

> En apprendre plus sur les [patrons de vues](https://birdy.alexisphilip.fr/#/tutorial/first-page?id=create-a-template).

## Multi-langues

Birdy permet de définir des traductions pour l'application.

Quand le multi-langue est activé, l'URL est réécrit (redirigé) :

Requête :

```text
domain.tld/controller_name
```

Redirection vers le language du navigateur, ou language par défaut si celui-ci n'est pas définit dans Birdy.

```text
domain.tld/LANGUAGE_CODE/controller_name
```

Après avoir [configuré le multi-langue](https://birdy.alexisphilip.fr/#/tutorial/multi-language?id=configuration), il faut
définir la [traduction des vues et des composants](https://birdy.alexisphilip.fr/#/tutorial/multi-language?id=translation-files)

Chaque vues doivent être traduites dans son fichier `.json` correspondant. Il est également possible de définir des 
[components](https://birdy.alexisphilip.fr/#/tutorial/multi-language?id=other-translations-components).

> En apprendre plus sur le [multi-langue](https://birdy.alexisphilip.fr/#/tutorial/multi-language).

@auteur_page : Alexis Philip