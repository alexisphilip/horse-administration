# Génération de données

> Les scripts de génération de données se trouvent dans le répertoire `queries_generate` et ne peuvent être executés 
depuis une requête HTTP. Si vous voulez les consulter, veuillez les télécharger depuis GitLab. 

Pour générer des données factices pour les 33 tables de la BDD, nous avons préféré créer nos propres scripts pour 
la majorité tables que d'utiliser l'outil [generatedata.com]().

Nous avons utilisé [generatedata.com]() pour la création de la table utilisateur, comportant de noms et des adresse.

Pour le reste des tables, nous avons créé nos scripts.

Avantages de créer son propre script :
- grande fléxibilité sur la génération de données ;
- facilité à gérer le taux de données aléatoires ;

?> Nous avons utilisé la librairie [php-lorem-ipsum](https://github.com/joshtronic/php-loremipsum) pour générer du texte
aléatoirement avec PHP.

## Données *naturelles*

Nous avons décidé de seulement générer 8 500 utilisateurs. *Pourquoi ?*

Pour rencontrer nos deux objectifs d'avoir au moins une base avec 1 000 000 de tuples tout en ayant des données réalistes
qui semblent naturelles, nous avons prit la décision que 8 500 utilisateurs seraient suffisants :

Chaque utilisateur a en moyenne 15 `establishment` (voir `queries_generate/establishment.php`) donc ~ 127 500 `establishment`.

Chaque establishment aura assigné à lui-même des `facility` (en fonction de leur capacité de stockage) (voir 
`queries_generate/establishment_facility.php`) donc ~ 500 000 `establishment_facility`.

Pour finir, chaque facility se verra assigner des chevaux ou des objets (toujours en fonction de leur capacité de stockage) (voir 
`queries_generate/facility_item.php`). Nous avons donc généré 1 100 000 de chevaux (avec generatedata.com) et assigné
 ~ 1,7 `object` à chacun d'entre eux (voir `queries_generate/horse_object.php`) donc ~ 1 800 000 de `horse_object`.

Si nous avions généré 100 000 ou 1 000 000 d'utilisateurs nous serions arrivés à des sommes astronomiques de données à générer 
que nous ne pouvions pas nous permettre avec nos VMs et VPS de cette puissance et notre capacité de stockage.

Avec 8 500 utilisateurs, nous avons 2 tables avec plus d'un milion de tuples. La consigne est respectée et les données
restent *naturelles* (semblent réelles).

## Exemples

### Connexions d'utilisateurs mensuelles

Dans la page d'acceuil [dashboard](https://horse-administration.alexisphilip.fr), nous pouvons voir que le graphique à 
gauche nommé `Annual user logging activity` affiche une courbe de variation beacoup plus intéressante que si les 
données étaient linéaires.

<div style="text-align:center"><img src="img/user-logs.png" alt="User log chart"></div>
<div style="text-align:center">Total annuel des connexions utilisateurs par mois</div>

Voici le script utilisé pour générer `x` logs pour chaques utilisateurs en fonction du temps entre `2008-01-01` et `2020-04-01`.

`queries_generate/user_log.php`

```php
$sql = "SELECT user_id FROM users";

$users = $db->exec($sql);

// For each users.
foreach ($users as $key => $user) {

	$random_ip = rand(1, 254) . "." . rand(1, 254) . "." . rand(1, 254) . "." . rand(1, 254);

	$sql = "
		INSERT INTO `user_log`(
			`user_id`,
			`user_history_ip`,
			`user_date_connection`)
		VALUES
	";
	
	// Generate bewteen 1 and 35 logs.
	$nb_logs = rand(1, 35);
	for ($y=0; $y < $nb_logs; $y++) {

		// Generate a random date between 2 dates.
		$timestamp_start = new DateTime("2008-01-01");
		$timestamp_end = new DateTime("2020-04-01");

		// Gets a random timestamp.
		$random_timestamp = rand($timestamp_start->getTimestamp(), $timestamp_end->getTimestamp());

		// Converts the random timestamp in date time.
		$sql_user_date_connection = date("Y-m-d H:i:s", $random_timestamp);

		$sql_user_id = $user["user_id"];

		// 1 / 7 the user will log into a new random IP.
		if (rand(1, 7) == 1) {
			$sql_user_history_ip = rand(1, 254) . "." . rand(1, 254) . "." . rand(1, 254) . "." . rand(1, 254);
		} else {
			$sql_user_history_ip = $random_ip;
		}

		$sql .= "
			($sql_user_id,
			\"$sql_user_history_ip\",
			\"$sql_user_date_connection\")
		";

		if ($y < $nb_logs - 1) {
			$sql .= ",";
		} else {
			$sql .= ";";
		}
	}
    
    // Execute query here.
	// print_r($sql);
	// print_r("<br>");
}
```

### Total des chevaux par types d'établissement.

Encore dans la page d'acceuil [dashboard](https://horse-administration.alexisphilip.fr), le graphique à droite nommé 
`Total horses per establishment types` montre un résultat réaliste : 86 % de chevaux en centre équestre et 14 % en 
compétition dans des hippodromes. 

<div style="text-align:center"><img src="img/horses-per-establishment-type.png" alt="User log chart"></div>
<div style="text-align:center">Total des chevaux par types d'établissement</div>

@auteur_page : Alexis Philip