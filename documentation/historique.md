# Historique du Projet

## Débuts
 
Comme tout bon début de projet, nous avons commencé par faire quelques brainstormings afin de se mettre d’accord sur les
technologies à utiliser, et bien sûr sur la réalisation du schéma relationnel de la base de données.

## Développement

Il nous a été demandé, pour la réalisation de ce projet, de faire appel à toutes nos connaissances acquises pendant
l’année (script bash, requêtes SQL, administration système et réseaux, etc.).

Nous avons dû être très organisés et pour cela nous avons mis en place un Git pour procéder au mieux au développement de
l’application.

## Calendrier

| Tâches réalisées                  | Dates de conception         | Date de rendu |
|-----------------------------------|-----------------------------|---------------|
| Schéma Relationnel                | du 17/01/2020 au 30/01/2020 | 31/01/2020    |
| Gestion des utilisateurs          | du 03/02/2020 au 07/02/2020 | 07/02/2020    |
| Interface d’administration        | du 03/02/2020 au 28/02/2020 | 28/02/2020    |
| Maintenance et surveillance       | du 02/03/2020 au 04/02/2020 | 06/03/2020    |
| Sauvegarde, réplication, sécurité | du 23/03/2020 au 27/03/2020 | 27/03/2020    |
| Livraison finale                  | du 01/04/2020 au 03/04/2020 | 03/04/2020    |

@auteur_page : Laura Sampaolo