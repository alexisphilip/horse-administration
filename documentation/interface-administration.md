# Interface d'administration

L'interface d'administration est la partie la plus conséquente de ce projet.

URL : https://horse-administration.alexisphilip.fr

![](img/admin-dashboard.png)

## ORM

Un ORM a été développé spécifiquement pour ce projet.

Nous allons appeler ce type d'ORM : *semi-automatique*.

?> Un ORM semi-automatique ?

Un ORM classique nécéssite de déclarer dans les modèles correspondant aux tables : les champs et leur type.

Ici, pas besoin de remplir des dizaines modèles, de contrôleurs et de vues pour les tables. Tout se fait automatiquement.

Dans l'interface d'administration, **pour afficher les 33 tables et leur details, il suffit d'uniquement** :
- 1 contrôleur (`Orm.php`) ;
- 1 modèle (`ORMModel.php`) ;
- 3 vues (`orm_list.php`, `orm_detail.php`, `orm_update.php`);

Voici en détail ce que font les trois fonctionnalités de l'ORM.

## ORM list

La fonctionanlité **list** de l'ORM utilise la méthode `Orm.php:list()` du contrôleur ORM, la méthode `ORMModel.php:findAll()`
du modèle ORM et la vue `orm_list.php`.

### Orm.php:`list()`

- param: **array(4)** `param`  Paramètres de l'URL :
  - **string** `name` Nom de la table.
  - **int** `limit` Nombre limite de tuples (optionel `[default=25]`).
  - **int** `page` Page limite basée sur le nombre de tuples (optionel `[default=1]`).
  - **array(2)** `where` Ajoute une déclaration WHERE pour une clé étrangère de la table (si il y en a une) (optionel `[default=[1, 1]]`).
    - **string** `[0]` Nom du champs FK de la table.
    - **int**    `[1]` Valeur.
- return: **void**

> Requête au modèle et affiche et les données d'une table.

Le routeur passe en paramètre à cette fonction les données de la table (`name` est obligatoire), exécute la méthode `findAll()` de `ORMModel.php`, et renvoie
les données à `orm_list.php` qui va construire un tableau en fonction des données renvoyées par `findAll()`.

Composition d'une requête et du formatage des paramètres :

```http
https://horse-administration.alexisphilip.fr/orm/list/name={nom_table}&limit={limite_tuple}&page={tuple_page}&where{nom_fk},{valeur_fk}
```

#### Exemples

Requête HTTP le minimum de paramètres sur `Orm.php:list()` :

Liste la première page des 25 premiers chevaux (`limit=25&page=1` sont des valeurs par défaut). 

```http
https://horse-administration.alexisphilip.fr/orm/list/name=horse
```

Requête HTTP avec tous les paramètres sur `Orm.php:list()` :

Liste la 3 ème page des 10 premiers chevaux de race ID = 1.

```http
https://horse-administration.alexisphilip.fr/orm/list/name=horse&limit=10&page=3&where=horse_breed_id,1
```

> En apprendre plus sur le [formatage de paramètres d'URL de Birdy](https://birdy.alexisphilip.fr/#/tutorial/fetching-data?id=from-url-parameters). 

### ORMModel.php:`findAll()`

- param: **string** `table_name` Nom de la table
- param: **int** `limit` Nombre limite de tuples.
- param: **int** `page` Page limite basée sur le nombre de tuples.
- param: **array(2)** `where` Ajoute une déclaration WHERE pour une clé étrangère de la table (si il y en a une).
    - **string** `[0]` Nom du champs FK de la table.
    - **int**    `[1]` Valeur.
- return: **array**

> Requête et renvoie les données d'une table.

Le contrôleur exécute cette méthode. Elle construit la requête en fonction des paramètres donnés et renvoie le résulat.

## ORM detail

La fonctionanlité **detail** de l'ORM utilise la méthode `Orm.php:detail()` du contrôleur ORM, la méthode `ORMModel.php:findOne()`
du modèle ORM et la vue `orm_detail.php`.

!> Il est important de noter que les vues détailées pour les tables `users`, `horse`, `establishment` et `newspaper` 
n'utilisent pas l'ORM. Elles passent par un contrôleur qui portent leur propre nom pour pouvoir créer une vue 
personnaliée et plus détaillée.
E.g.: `https://horse-administration.alexisphilip.fr/users/list/id=1`

### Orm.php:`detail()`

- param: **array(2)** `param`  Paramètres de l'URL :
  - **string** `name` Nom de la table.
  - **int** `id` Valeur PK de la table.
- return: **void**

> Requête au modèle et affiche et les données d'une table en fonction de la clé primaire.

Le routeur passe en paramètre à cette fonction les données de la table (`name` et `id`), exécute la méthode `findOne()` de `ORMModel.php`, et renvoie
les données à `orm_detail.php` qui va construire un tableau en fonction des données renvoyées par `findOne()`.

Composition d'une requête et du formatage des paramètres :

```http
https://horse-administration.alexisphilip.fr/orm/detail/name={nom_table}&id={valeur_pk}
```

#### Exemple

Requête HTTP sur `Orm.php:detail()` :

Détail du concours d'hippodrome numéro 42 944.

```http
https://horse-administration.alexisphilip.fr/orm/detail/name=hippodrome_contest&id=42944
```

### ORMModel.php:`findOne()`

- param: **string** `name` Nom de la table.
- param: **int** `id` Valeur PK de la table.
- return: **array**

> Requête et renvoie les données d'une table en fonction de la clé primaire.

Le contrôleur exécute cette méthode. Elle construit la requête en fonction des paramètres donnés et renvoie le résulat.


## ORM update

La fonctionanlité **update** de l'ORM utilise la méthode `Orm.php:update()` du contrôleur ORM, la méthode `ORMModel.php:update()`
du modèle ORM et la vue `orm_update.php`.

!> Attention : cette caractéristique n'est pas encore fonctionnelle. Elle ne fait que récupérer tous les champs, 
analyse les types, les valeurs, et les affiche dans champs d'entrée propre à eux. Il n'y pas encore de moyen de publier
les changements effectués.

### Orm.php:update()

- param: **array(2)** `param`  Paramètres de l'URL :
  - **string** `name` Nom de la table.
  - **int** `id` Valeur PK de la table.
- return: **void**

> Requête au modèle et affiche et les données d'une table en fonction de la clé primaire.

Le routeur passe en paramètre à cette fonction les données de la table (`name` et `id`), exécute la méthode `update()` de `ORMModel.php`, et renvoie
les données à `orm_update.php` qui va construire un formulaire de modification en fonction des données renvoyées par `update()`.

Composition d'une requête et du formatage des paramètres :

```http
https://horse-administration.alexisphilip.fr/orm/update/name={nom_table}&id={valeur_pk}
```

#### Exemple

Requête HTTP sur `Orm.php:update()` :

Formulaire de modification du concours d'hippodrome numéro 42 944.

```http
https://horse-administration.alexisphilip.fr/orm/update/name=hippodrome_contest&id=42944
```

### ORMModel.php:update()

- param: **string** `name` Nom de la table.
- param: **int** `id` Valeur PK de la table.
- return: **array**

> Requête et renvoie les données d'une table en fonction de la clé primaire.

Le contrôleur exécute cette méthode. Elle construit la requête en fonction des paramètres donnés et renvoie le résulat.

#### Exemple valeur de retour

```php
Array() [
   [   // Correponds à un champs input[type="number",min=1,max=100"].
       "name" => "horse_disease_id",
       "value" => 100,
       "type" => "int",
       "option" => false,
       "range" => [1, 100]
   ],
   [   // Correponds à un champs select avec trois valeurs dont 7502 pré-sélectionée.
       "name" => "horse_id",
       "value" => 7502,
       "type" => "int",
       "option" => [7501, 7502, 7503],
       "range" => false
   ],
   [   // Corresponds à un champs input[type="text",min=1,max=255"].
       "name" => "horse_name",
       "value" => "Ah que coucou Bob",
       "type" => "string",
       "option" => false,
       "range" => [1, 255]
   ],
   [   // Corresponds à un champs input[type="textarea"].
       "name" => "horse_description",
       "value" => "Salut Patrick",
       "type" => "string",
       "option" => false,
       "range" => false
   ]
];
```

## Gestion des utilistateurs

Une fois les utilisateurs créés, ils déclarés dans la configuration de la base de données du framework Birdy.

Il suffit de déclarer tous les utilisateurs de cette manière-ci, puis appeller depuis le modèle le mot-clé qui le définit
pour instancer une connexion PDO avec et utilisateur.

`app/config/database.php` (Mots de passe retirés pour la démonstration.)

```php
/*
 * Admins.
 */
$database["admin"] = [
    "host" => "localhost",
    "db"   => "horse-administration",
    "user" => "horse-admin",
    "pass" => ""
];

$database["admin-privilege"] = [
    "host" => "localhost",
    "db"   => "horse-administration",
    "user" => "horse-admin-privilege",
    "pass" => ""
];

$database["admin-contest"] = [
    "host" => "localhost",
    "db"   => "horse-administration",
    "user" => "horse-admin-contest",
    "pass" => ""
];
``` 

> Pour en savoir plus, veuillez vous référer à la [section sur la connexion des utilisateurs MySQL](https://horse-administration.alexisphilip.fr/docs/#/framework-php?id=connexions-bases-de-donn%c3%a9es-multiples).

!> L'ORM n'est pas encore configuré pour la gestion des utilisateur : toutes les requêtes sur les tables sont faites avec
l'utilisateur `horse-admin`. Il faudra rajouter dans le modèle `ORMModel.php` une condition qui utilisera le correct utilisateur
MySQL en fonction du nom de la table sur laquelle la requête est faite.

## Framework PHP Birdy

Veuillez vous référer à la [section sur le framework Birdy](https://horse-administration.alexisphilip.fr/docs/#/framework-php) pour toutes les information.

@auteur_page : Alexis Philip