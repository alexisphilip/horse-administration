# Introduction

<div style="text-align:center"><img src="img/logo.png" alt="Horse administration logo"></div>

Pour la validation de notre Licence Professionnelle : Web, E-commerce et Big data, il nous a été demandé de réaliser un système d’information complet pour un jeu Web de simulation de chevaux en PHP et MySQL. L’application devait présenter les différents concepts vus tout au long des cours de Système d’Information et Base de Données.

Le cahier des charges techniques simplifié devait s’inspirer de ceux donné en cours.

**Interface d'administration** : https://horse-administration.alexisphilip.fr

**PHPMyAdmin** : https://phpmyadmin.alexisphilip.fr

**GitLab** : https://gitlab.com/alexis-philip/horse-administration

## Points clés du projet

- Concevoir une base de données avec une extension très conséquente ;
- Une mise en place d’une politique de gestion d’utilisateurs ;
- Une interface d’administration ;
- Une politique de maintenance et de surveillance (y compris de journalisation) permettant de vérifier l’ensemble des statistiques d’utilisation du serveur et donc de faire à tout instant :
  - un diagnostic,
  - une inspection,
  - une défragmentation,
  - et une optimisation ;
- Une politique de sauvegarde, de réplication et de sécurité.

@auteur_page : Laura Sampaolo, Wasner Nicolas