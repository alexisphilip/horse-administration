# Liens et ressources

## Ressources

Maintenance et surveillance

https://www.mickael-martin-nevot.com/univ-amu/iut/lp-web/systeme-d-information-et-bd/s04-cm4-maintenance-et-surveillance.pdf

Sauvegarde, réplication et securité

https://www.mickael-martin-nevot.com/univ-amu/iut/lp-web/systeme-d-information-et-bd/s04-cm5-sauvegarde-replication-et-securite.pdf

Maintenance et surveillance

https://www.mickael-martin-nevot.com/univ-amu/iut/lp-web/systeme-d-information-et-bd/s04-cm4-maintenance-et-surveillance.pdf

Framework Birdy

https://birdy.alexisphilip.fr

Documentation réalisée avec *docsify*.

https://docsify.js.org/#/

## Liens

Interface d'administration

https://horse-administration.alexisphilip.fr

PHPMyAdmin

https://phpmyadmin.alexisphilip.fr

GitLab

https://gitlab.com/alexis-philip/horse-administration


## Auteurs

### Rédaction

- Alexis Philip
- Laura Sampaolo
- Nicolas Wasner

### Mise en page et création de la documentation docsify

- Alexis Philip


Vous êtes arrivé jusque-là ? Regardez le README.md and `app/view/old_views/`.
