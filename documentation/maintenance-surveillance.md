# Maintenance et surveillance

Les tâches sont des workflows définis qui exécutent un ensemble de tâches de maintenance préconfigurées, telles que la 
sauvegarde de la base de données, la journalisation d’informations ou l’exécution de jobs SQL Server.

Les tâches sont configurées pour être exécutées dans un ordre spécifique ou regroupé dans des sous-plans et planifiées
pour être exécutées à différents moments.

## Maintenance

La maintenance informatique désigne, en génie logiciel, des modifications apportées à un logiciel, après sa mise en 
oeuvre, pour corriger les fautes, en améliorer l’efficacité ou autres caractéristiques, ou encore adapter celui-ci à un 
environnement modifié.

`script_inspection.sh`

```bash
#!/bin/bash

myisamchk -dvv --silent --fast /var/lib/mysql/horse/*.MYI > "myisam_table.log";
mysqlcheck horse > "database.log";
result = `grep -c "error" database.log`

if [$result > 0] bash script_reparation; fi
```

`script_reparation.sh`

```bash
#!/bin/bash

myisamchk /var/lib/mysql/horse/*.MYI > "myisam_table.log";
mysqlcheck horse > "all_table.log";
myisamchk --force --recover --fast --update-state /var/lib/mysql/horse/*.MYI > "table_repair.log";

/etc/init.d/mysql restart;
```

## Journalisation

En informatique, le concept d’historique des événements ou de journalisation désigne l’enregistrement séquentiel dans 
un fichier ou une base de données de tous les événement affectant un processus particulier (activité d’un réseau 
informatique, les potentielles erreurs survenues, etc.).

Le journal ou log désigne alors le fichier contenant ces enregistrements. Généralement datés et classés par ordre 
chronologique, ces derniers permettent d’analyser pas à pas l’activité interne du processus et ses interactions avec 
son environnement.

`script_journalisation.sh`

```bash
#!/bin/bash

mysqld --log --datadir mysqllog
mysqld --log-error --datadir mysqlerror
mysqld --log-bin --datadir mysqlbin
mysqld --log-isam --datadir mysqlisam
mysqld --log-slow-query --datadir mysqlslowqquery

LOG_FILE=/var/lib/mysql/mysqllog.log
ERR_FILE=/var/lib/mysql/mysqlerror.log
BIN_FILE=/var/lib/mysql/mysqlbin.log
ISAM_FILE=/var/lib/mysql/mysqlisam.log
SLOW_QUERY_FILE=/var/lib/mysql/mysqlslowqquery.log

tail $LOG_FILE $ERR_FILE $BIN_FILE $ISAM_FILE $SLOW_QUERY_FILE > ~/global.log 

cat global.log
```

@auteur_page : Laura Sampaolo