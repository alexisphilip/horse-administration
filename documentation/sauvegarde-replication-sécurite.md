# Sauvegarde, réplication et sécurité

## Sauvegarde

La sauvegarde est l’opération qui consiste à dupliquer et à mettre en sécurité des données contenues dans un système 
informatique.

Pour sauvegarder notre base de données, nous avons décidé d’utiliser `MySQLDump` car il sert à écrire un fichier 
contenant toutes les directives SQL permettant de recréer une base de données à l’identique de l’état dans lequel elle
se trouvait au moment de la sauvegarde.

`script_save.sh`

```bash
#!/bin/sh

# Configuration de l'utilisateur MySQL et de son mot de passe.
DB_USER="root"
DB_PASS="mot_de_passe123"

# Configuration de la machine hébergeant le serveur MySQL.
DB_HOST="localhost"

# Sous-chemin de destination.
OUTDIR=`date +%Y-%m-%d/%H:%M:%S`

# Création de l'arborescence.
mkdir -p /var/archives/$OUTDIR

# Récupération de la liste des bases.
DATABASES=`MYSQL_PWD=$DB_PASS mysql -u $DB_USER -e "SHOW DATABASES;" | tr -d "| " | grep -v -e Database -e _schema -e mysql`

# Sauvegarde de la base.
mysqldump --opt database > backup-file.sql
mysqldump --opt database | mysql ---host=remote-host -C database
mysql -u root -p mot_de_passe123 -e "source /patch/backup-file.sql" database
```

## Réplication

La réplication est un processus de partage d’informations pour assurer la cohérence de données entre plusieurs sources 
de données redondantes, pour améliorer la fiabilité, la tolérance aux pannes, ou la disponibilité. On parle de 
réplication de données si les mêmes données sont dupliquées sur plusieurs périphériques.

La réplication n’est pas à confondre avec une sauvegarde : les données sauvegardées ne changent pas dans le temps, 
reflétant un état fixe des données, tandis que les données répliquées évoluent sans cesse à mesure que les données 
sources changent.

`script_replication.sh`

```bash
# Attribution des droits sur le maître et l'esclave.
GRANT REPLICATION SLAVE, SELECT, SUPER, RELOAD ON *.* TO replication@'%'
IDENTIFIED BY 'replication';

# Configuration du my.conf du maître.
# Activation des logs binaires.
log-bin=/var/log/mysql-bin.log
# Définition de l'identifiant unique.
server-id=1

# Réplique de la base de données.
replicate-do-db=database
# Réplique les requêtes multi-bases.
replicate-wild-do-table=cdatabase.%

# Configuration du my.conf de l'esclave.
log-bin=/var/log/mysql-bin.log
# Définition de l'identifiant unique.
server-id=2

# Nom d'hôte du maître
master-host=master_server
# Port sur lequel écoute le serveurs maître.
master-port=3306
# Nom utilisé pour se connecter au maître.
master-user=replication
# Mot de passe pour se connecter au maître.
master-password=replication

# Configuration de l'esclave.

# Créer la base à répliquer.
CREATE DATABASE nom_de_la_base;
# Copier les données depuis le maître.
LOAD DATA FROM MASTER;
# Démarrer le processus esclave.
START SLAVE;

# Réplication : gestion de panne.

# Arrêter le process esclave.
STOP SLAVE
# Transformer le processus esclave en maître.
RESET MASTER

# Ancien maître devient esclave.
CHANGE MASTER TO
 MASTER_HOST='slave_server',
 MASTER_PORT=3306,
 MASTER_USER='replication',
 MASTER_PASSWORD='replication';

# Réaffecter l'ancien maître restauré.
RESET MASTER;

# Ancien esclave redevient esclave.
CHANGE MASTER TO
 MASTER_HOST='master_server',
 MASTER_PORT=3306,
 MASTER_USER='replication',
 MASTER_PASSWORD='replication';

# Démarrer le processus esclave.
START SLAVE
```

## Sécurité générale

La sécurité informatique est une discipline qui se veut de protéger l’intégrité et la confidentialité des informations 
stockées dans un système informatique. Quoi qu’il en soit, il n’existe aucune technique capable d’assurer 
l’inviolabilité d’un système.

```bash
# Sécurité Générale.
# /!\ Mettre un mdp a root et pas le laisser par défaut.
#     Ne donner que les privilèges nécessaires aux utilisateurs.

# Supprimer le second compte administrateur (inutile).
DELETE FROM user WHERE Host='hostname' AND user='root';

# Interdire les connexions non identifiées.
DELETE FROM user WHERE Password='';

# Supprimer la base test et toutes les bases commençant par test_ car elles sont accessibles par tout le monde.
DELETE FROM db WHERE db='test' OR db='test\_%';

# Dans le fichier my.conf :
# un utilisateur ne doit pouvoir lister que les bases de données dans lesquelles il a des droits.
safe-show-database

# Interdire les connexions distantes (si possible).
bind-address = 127.0.0.1

# Limiter le nombre de connexion simultanées.
max_connection=100 #(100 connexions max)
max_user_connections=50 #(1 utilisateur a le droit à 50 connexions max)
```

@auteur_page : Laura Sampaolo