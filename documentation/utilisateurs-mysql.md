# Utilisateurs MySQL

## Création des utilisateur

Nous avons du créer 10 utilisateurs, tous ayant des rôles différents sur l'interface d'administration.

!> Règle : tous les utilisateurs MySQL de l'application commencent par `horse-`, sont en **minuscules**, et les mots
séparés par des **traits d'union**. 

Requêtes de création :

```sql
# Adminitrateur
CREATE USER 'horse-admin'@'localhost' IDENTIFIED BY 'HorseAdministration';
# Adminitrateur de privilèges
CREATE USER 'horse-admin-privilege'@'localhost' IDENTIFIED BY 'HorseAdministration';
# Responsable optimisation
CREATE USER 'horse-manager-index'@'localhost' IDENTIFIED BY 'HorseAdministration';
# Tache automatisée
CREATE USER 'horse-automated-task'@'localhost' IDENTIFIED BY 'HorseAdministration';
# Développeur
CREATE USER 'horse-dev'@'localhost' IDENTIFIED BY 'HorseAdministration';
# Modérateur de communauté
CREATE USER 'horse-moderator'@'localhost' IDENTIFIED BY 'HorseAdministration';
# Spécialiste métier
CREATE USER 'horse-job-specialist'@'localhost' IDENTIFIED BY 'HorseAdministration';
# Administrateur de concours
CREATE USER 'horse-admin-contest'@'localhost' IDENTIFIED BY 'HorseAdministration';
# Journaliste
CREATE USER 'horse-admin-columnist'@'localhost' IDENTIFIED BY 'HorseAdministration';
# Client (utilistateur du jeu)
CREATE USER 'horse-client'@'localhost' IDENTIFIED BY 'HorseAdministration';
```

!> Étant sur MariaDB 10.1.44 (voir capture d'écran ci-dessous), nous n'avons pas réussis créer les utilisteurs avec 
la déclaration `GRANT` suivi de `IDENTIFIED BY`. Nous pensons que cette version de MariaDB ne supporte pas encore
cette fonctonnalité. Nous avons donc créé les utilisateur avec `CREATE USER`. 

![](img/maria-version.png)


## Ajout de droits

Requêtes d'ajout de droits :

```sql
# Adminitrateur
GRANT ALL PRIVILEGES ON *.* TO 'horse-admin'@'localhost';
# Adminitrateur de privilèges
GRANT USAGE ON *.* TO 'horse-admin-privilege'@'localhost' WITH GRANT OPTION;
# Responsable optimisation
GRANT INDEX, ALTER ON *.* TO 'horse-manager-index'@'localhost';
# Tache automatisée
GRANT RELOAD, SHUTDOWN, PROCESS, SUPER ON *.* TO 'horse-automated-task'@'localhost';
# Développeur
GRANT SELECT, INSERT, UPDATE, DELETE ON `horse-administration`.* TO 'horse-dev'@'localhost';
# Modérateur de communauté
GRANT UPDATE, DELETE ON `horse-administration`.users TO 'horse-moderator'@'localhost';
# Spécialiste métier
GRANT SELECT, UPDATE ON `horse-administration`.horse TO 'horse-job-specialist'@'localhost';
# Administrateur de concours
GRANT SELECT, INSERT, UPDATE, DELETE ON `horse-administration`.hippodrome_contest TO 'horse-admin-contest'@'localhost';
GRANT SELECT, INSERT, UPDATE, DELETE ON `horse-administration`.contest_prize TO 'horse-admin-contest'@'localhost';
GRANT SELECT, INSERT, UPDATE, DELETE ON `horse-administration`.contest TO 'horse-admin-contest'@'localhost';
# Journaliste
GRANT SELECT, INSERT, UPDATE, DELETE ON `horse-administration`.newspaper TO 'horse-admin-columnist'@'localhost';
GRANT SELECT, INSERT, UPDATE, DELETE ON `horse-administration`.newspaper_article TO 'horse-admin-columnist'@'localhost';
GRANT SELECT, INSERT, UPDATE, DELETE ON `horse-administration`.newspaper_advertisement TO 'horse-admin-columnist'@'localhost';
GRANT SELECT, INSERT, UPDATE, DELETE ON `horse-administration`.advertisement TO 'horse-admin-columnist'@'localhost';
# Client (utilistateur du jeu)
GRANT SELECT ON `horse-administration`.newspaper TO 'horse-client'@'localhost';
GRANT SELECT ON `horse-administration`.newspaper_article TO 'horse-client'@'localhost';
GRANT SELECT ON `horse-administration`.newspaper_advertisement TO 'horse-client'@'localhost';
GRANT SELECT ON `horse-administration`.advertisement TO 'horse-client'@'localhost';
GRANT SELECT ON `horse-administration`.contest TO 'horse-client'@'localhost';
GRANT SELECT ON `horse-administration`.contest_prize TO 'horse-client'@'localhost';
GRANT SELECT ON `horse-administration`.hippodrome_contest TO 'horse-client'@'localhost';
```

@auteur_page : Alexis Philip
