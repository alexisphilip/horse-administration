# Queries generate

These files generate queries to populate by `n` tuples the tables from the database.

That's it.

Really.

Checkout what they do: [documentation](https://horse-administration.alexisphilip.fr/docs/#/generation-donnees).