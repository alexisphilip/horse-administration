<?php


include("db_connect.php");


$sql = "SELECT object_id FROM object";
$objects = $db->exec($sql);


$sql = "SELECT hippodrome_contest_id FROM hippodrome_contest";
$hippodrome_contests = $db->exec($sql);


$total_objects = count($objects);
$total_hippodrome_contests = count($hippodrome_contests);

// For each contests
foreach ($hippodrome_contests as $key => $value) {

	$sql = "
		INSERT INTO `contest_prize`(
			`hippodrome_contest_id`,
			`object_id`)
		VALUES
	";

	// Assigns one or multiple objects to each contests.
	$nb_objects = rand(1, 4);
	for ($i=0; $i < $nb_objects; $i++) {

		$sql_hippodrome_contest_id = $hippodrome_contests[rand(1, $total_hippodrome_contests - 1)]["hippodrome_contest_id"];
		$sql_object_id = rand(1, $total_objects);

		$sql .= "
			($sql_hippodrome_contest_id,
			$sql_object_id)
		";

		if ($i < $nb_objects - 1) {
			$sql .= ",";
		} else {
			$sql .= ";";
		}

	}

	// print_r($sql);
	// print_r("<br>");

}

