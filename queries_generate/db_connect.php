<?php

class Db
{
	private $pdo;

	public function __construct()
	{
		// $pdo = new PDO('mysql:host=localhost;dbname=horse-administration', "horse-admin", "HorseAdministration");
	}

	public function exec($query)
	{
		$stmt = $this->pdo->prepare($query);

		$stmt->execute();

		return $stmt->fetchAll(PDO::FETCH_ASSOC);
	}
}

$db = new Db();
