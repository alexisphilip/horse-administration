<?php

include("db_connect.php");


$sql = "SELECT user_id FROM users";

$users = $db->exec($sql);


$ai = 1;

// For each users.
foreach ($users as $key => $value) {

	// echo "user ID: " . $value["user_id"];
	// echo "<br><br><br>";


	// NUMBER OF ESTABLISHMENTS EACH
	for ($i=0; $i < rand(1, 30); $i++) { 
		

		// ESTABLISHMENT TYPE
		// 1/3 "hippodrome".
		if (rand(1, 3) === 1) {
			$type = 2;
		} // 2/3 "equestrian_center".
		else {
			$type = 1;
		}


		// ESTABLISHMENT CAPACITY
		// For "equestrian center" : between 30 and 100.
		if ($type === 1) {
			$random_capacity = rand(30, 300);
		}
		// For "hippodrome" : between 10 and 50.
		else {
			$random_capacity = rand(20, 60);
		}
		// Rounded to the nearest fifth.
		$capacity = round(($random_capacity + 5 / 2) / 5) * 5 - 5;


		// ESTABLISHMENT PRICE
		// Capacity * 12,000 rounded to the nearest thousandth.
		$price = round($capacity * 12000, -3);

		// echo "type: " . $type;
		// echo "<br>";
		// echo "capacity: " . $capacity;
		// echo "<br>";
		// echo "price: " . $price;
		// echo "<br><br>";

		$sql_auto_increment = $ai;
		$sql_user_id = $value['user_id'];
		$sql_type = $type;
		$sql_capacity = $capacity;
		$sql_price = $price;

		$sql = "
			INSERT INTO `establishment`(
				`establishment_id`,
				`establishment_owner_id`,
				`establishment_type_id`,
				`establishment_capacity`,
				`establishment_price`)
			VALUES (
				$sql_auto_increment,
				$sql_user_id,
				$sql_type,
				$sql_capacity,
				$sql_price);
		";

		print_r($sql);
		// echo "<br>";
	
		$ai++;

	}

	// echo "<hr><br>";
}
