<?php

include("db_connect.php");



$sql = "SELECT * FROM establishment LIMIT 55000, 5000";
$establishments = $db->exec($sql);

$sql = "SELECT f.*, ff.* FROM facility f LEFT JOIN facility_family ff ON ff.facility_family_id = f.facility_family_id";
$facilities = $db->exec($sql);



$ai = 1;

// For each establishments, assign facilities to it.
foreach ($establishments as $key => $value) {

	$establishment_id = $value["establishment_id"];
	$type = $value["establishment_type_id"];
	$capacity = $value["establishment_capacity"];


	// echo "establishment type: " . $type . "<br><br>";

	// CAPACITY

	$total_facility_capacity = 0;
	$facility_ids = [];

	// Picks up random facilities until their total
	// capacity matches the establishment's capacity.
	while ($total_facility_capacity <= $capacity) {

		// Selects a random facility.
		$rand_facility_id = rand(1, count($facilities));
		$facility = $facilities[$rand_facility_id - 1];

		$total_facility_capacity += $facility["facility_capacity"];
		$facility_ids[] = $facility["facility_id"];

		if ($total_facility_capacity > $capacity) {
			$total_facility_capacity -= $facility["facility_capacity"];
			$facility_ids[] = array_pop($facility_ids);

			if (count($facility_ids) > 1) {				
				break;
			}
		}

		// echo "-- " . $facility["facility_id"] . " " . $facility["facility_family_name"] . " lvl " . $facility["facility_level"] . "<br>";
		// echo "-- capacity: " . $facility["facility_capacity"] . "<br><br>";
	}

	// print_r($facility_ids);
	// echo "<br>max capacity: " . $capacity . "<br>";
	// echo "current capacity: " . $total_facility_capacity . "<br><br>";
	// echo "<hr><br>";

	$sql_establishment_id = $establishment_id;


	// $sql = "
	// 	INSERT INTO `establishment_facility`(
	// 		`establishment_facility_id`,
	// 		`establishment_id`,
	// 		`facility_id`)
	// 	VALUES
	// ";
	$sql = "
		INSERT INTO `establishment_facility`(
			`establishment_id`,
			`facility_id`)
		VALUES
	";

	// For each facilities found, create a query.
	for ($i=0; $i < count($facility_ids); $i++) { 

		$sql_autoincrement = $ai;
		$sql_facility_id = $facility_ids[$i];

		// $sql .= "
		// 	($sql_autoincrement,
		// 	$sql_establishment_id,
		// 	$sql_facility_id)
		// ";
		$sql .= "
			($sql_establishment_id,
			$sql_facility_id)
		";

		if ($i < count($facility_ids) - 1) {
			$sql .= ",";
		} else {
			$sql .= ";";
		}

		$ai++;
	}

	echo $sql;
	echo "<br>";

}
