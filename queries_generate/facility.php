<?php

include("db_connect.php");


$sql = "SELECT * FROM facility_family";
$facility_families = $db->exec($sql);


$ai = 1;

// For each facilty families.
foreach ($facility_families as $key => $value) {

	// Create 3 levels per facilities.
	for ($i=1; $i <= 3; $i++) { 

		// FACILITY FAMILY
		$family_id = $value["facility_family_id"];
		$family_name = $value["facility_family_name"];
	
		// FACILITY LEVEL
		$level = $i;

		// FACILITY CAPACITY
		// FACILITY PRICE
		switch ($family_name) {
			case "track":
				$capacity = 3 * $level;
				$price = round(7000 * $level, -2);
				break;
			case "stable";
				$capacity = 8 * $level;
				$price = round(12000 * $level, -2);
				break;
			case "paddock";
				$capacity = 12 * $level;
				$price = round(10000 * $level, -2);
				break;
			case "storage_shed";
				$capacity = 10 * $level;
				$price = round(8000 * $level, -2);
				break;
			case "washing_station";
				$capacity = 5 * $level;
				$price = round(16000 * $level, -2);
				break;
			case "contest_track";
				$capacity = round(6 * ($level * 0.9), 0);
				$price = round(92000 * $level, -2);
				break;
		}

		// echo "level: " . $level . "<br>";
		// echo "family id: " . $family_id . "<br>";
		// echo "family name: " . $family_name . "<br>";
		// echo "capacity: " . $capacity . "<br>";
		// echo "price: " . $price . "<br>";

		$sql_auto_increment = $ai;
		$sql_facility_family_id = $family_id;
		$sql_facility_level = $level;
		$sql_facility_capacity = $capacity;
		$sql_facility_price = $price;

		$sql = "
			INSERT INTO `facility`(
				`facility_id`,
				`facility_family_id`,
				`facility_level`,
				`facility_capacity`,
				`facility_price`)
			VALUES (
				$sql_auto_increment,
				$sql_facility_family_id,
				$sql_facility_level,
				$sql_facility_capacity,
				$sql_facility_price);
		";

		echo $sql;
		echo "<br>";

		$ai++;
	}

	// echo "<hr><br>";
}
