<?php

include("db_connect.php");



$sql = "
	SELECT
		ef.establishment_facility_id,
		ef.facility_id,
		ff.facility_family_name,
		et.entity_type_name
	FROM establishment_facility ef
	LEFT JOIN facility f ON f.facility_id = ef.facility_id
	LEFT JOIN facility_family ff ON ff.facility_family_id = f.facility_family_id
	LEFT JOIN entity_type et ON et.entity_type_id = ff.entity_type_id
	WHERE ff.entity_type_id = 1
	ORDER BY ef.establishment_facility_id
	LIMIT 93859‬, 31286
";
// 3 horses
// LIMIT 0, 31286
// LIMIT 31286, 31286
// LIMIT 62573, 31286
// LIMIT 93859‬, 31286
// LIMIT 125146, 31286
// LIMIT 156432, 31286
// LIMIT 187719, 31286
// LIMIT 219005, 31289

// 2 horses
// LIMIT 250294, 174559
$facilities = $db->exec($sql);


$sql = "
	SELECT *
	FROM horse
	LIMIT 281580, 93860
";
// 3 horses
// LIMIT 0, 93860
// LIMIT 93860, 93860
// LIMIT 187720, 93860
// LIMIT 281580, 93860
// LIMIT 375441, 93860
// LIMIT 469301, 93860
// LIMIT 563161, 93860
// LIMIT 657021, 93861

// 2 horses
// LIMIT 750882, 349118‬
$horses = $db->exec($sql);


// TODO for each of facilities instances (establishment_facility_id)

// if "entity_type_name" == "horse" -> add horse to "facility_item_object_id"
// else == "object" -> add object to "facility_item_object_id"

// Total entities : 509580
// Horse entities : 424853
// Object entities:  84727

// Horses

// 3 horses to: 250 294 (750 882)
// 2 horses to: 174 559‬ (349 118‬)



// Auto increment represents the horse's ID.
$ai = 1;

// For each facilities.
foreach ($facilities as $key => $value) {

	$sql = "
		INSERT INTO `facility_item`(
			`establishment_facility_id`,
			`facility_item_horse_id`,
			`facility_item_object_id`)
		VALUES
	";

	// Insert 3 horses.
	for ($i=0; $i < 3; $i++) {

		$sql_establishment_facility_id = $value["establishment_facility_id"];
		$sql_facility_item_horse_id = $horses[$ai - 1]["horse_id"];
		$sql_facility_item_object_id = "NULL";

		$sql .= "
			($sql_establishment_facility_id,
			$sql_facility_item_horse_id,
			$sql_facility_item_object_id)
		";

		if ($i < 2) {
			$sql .= ",";
		} else {
			$sql .= ";";
		}

		$ai++;
	}

	// $db->exec($sql);
	print_r($sql);
}

