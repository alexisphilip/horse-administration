<?php

include("db_connect.php");
include("LoremIpsum.php");


// Generates old newspaper until today 2020-03-31.
// ###############################################


// Generates newspapers from 2008-01-01 until 2020-03-31.
$date = strtotime("2007-12-31");

$number_of_newspapers = 4474;

// for ($i=0; $i < $number_of_newspapers; $i++) {
// 	$date = strtotime("+1 day", strtotime(date("Y-m-d", $date)));
// 	$string_date = date("Y-m-d", $date);
// 	// echo date("Y-m-d", $date);
// 	// echo "<br>";

// 	$sql_newspaper_id = $i + 1;
// 	$sql_newspaper_release_date = $string_date;

// 	$sql = "
// 		INSERT INTO newspaper (
// 			newspaper_id,
// 			newspaper_release_date)
// 		VALUES (
// 			$sql_newspaper_id,
// 			\"$sql_newspaper_release_date\");
// 	";
// 	// print_r($sql);
// 	// print_r("<br>");
// }





$number_of_newspapers = 4474;

// For each news papers.
// Generates from 2 to 3 random articles per newspaper.
for ($y=0; $y < $number_of_newspapers; $y++) {

	for ($h=0; $h < rand(2, 4); $h++) { 
		
		// Random word picker.
		$lipsum = new LoremIpsum();


		// TITLE
		$words = $lipsum->wordsArray(100);

		$random_keys = (array_rand($words, rand(4, 6)));

		for ($i=0; $i < count($random_keys); $i++) { 
			$picked_words[] = $words[$random_keys[$i]];
		}

		$article_title = ucfirst(join(" ", $picked_words));


		// CONTENT (with HTML <p> around them).
		$article_content = $lipsum->paragraphs(rand(2, 3), "p");



		// AUTHOR
		$words = $lipsum->wordsArray(100);

		$random_keys = (array_rand($words, 2));
		$picked_words = [];

		for ($i=0; $i < count($random_keys); $i++) { 
			$picked_words[] = ucfirst($words[$random_keys[$i]]);
		}

		$article_author = join(" ", $picked_words);


		$sql_newspaper_id = $y + 1;
		$sql_newspaper_article_title = $article_title;
		$sql_newspaper_article_content = $article_content;
		$sql_newspaper_article_author = $article_author;

		$sql = "
			INSERT INTO newspaper_article (
				newspaper_id,
				newspaper_article_title,
				newspaper_article_content,
				newspaper_article_author)
			VALUES (
				$sql_newspaper_id,
				\"$sql_newspaper_article_title\",
				\"$sql_newspaper_article_content\",
				\"$sql_newspaper_article_author\");
		";
		print_r($sql);
		// print_r("<br>");
	}

}



// Randomly picks 2 advertisements and links them to the current newspaper.

// $sql = "SELECT advertisement_id FROM advertisement";
// $advertisements = $db->exec($sql);

// // For each newspapers.
// for ($y=0; $y < $number_of_newspapers; $y++) { 

// 	// Insert 2 advertisements.
// 	for ($i=0; $i < 2; $i++) { 

// 		$sql_newspaper_id = $y + 1;
// 		$sql_advertisement_id = $advertisements[rand(1, count($advertisements)) - 1]["advertisement_id"];

// 		$sql = "
// 		INSERT INTO newspaper_advertisement (
// 			newspaper_id,
// 			advertisement_id)
// 		VALUES (
// 			$sql_newspaper_id,
// 			$sql_advertisement_id);
// 		";

// 		// print_r($sql);
// 		// print_r("<br>");
// 	}
// }




// $lipsum->words(5);
// $lipsum->sentences(5);
// $lipsum->paragraphs(5);

