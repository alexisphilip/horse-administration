<?php


include("db_connect.php");


$sql = "SELECT contest_id FROM contest";
$contests = $db->exec($sql);


$sql = "SELECT establishment_id FROM establishment WHERE establishment_type_id = 2";
$establishments = $db->exec($sql);


$contest_length = count($contests);
$establishment_length = count($establishments);


// Adds a certain number of contests.
for ($i=0; $i < 40000; $i++) {

	$timestamp_start = new DateTime("2008-01-01");
	$timestamp_end = new DateTime("2021-01-01");

	// Gets a random timestamp.
	$random_timestamp = rand($timestamp_start->getTimestamp(), $timestamp_end->getTimestamp());

	// Converts the random timestamp in date time.
	$sql_hippodrome_contest_date_start = date("Y-m-d H:i:s", $random_timestamp);
	// Adds between 1 to 6 hours to the random timestamp and converts it to date time.
	$sql_hippodrome_contest_date_end = date("Y-m-d H:i:s", ($random_timestamp + (rand(1, 6) * 3600)));

	// Picks up random contest and establishments IDs.
	$sql_contest_id = $contests[rand(1, $contest_length) - 1]["contest_id"];
	$sql_establishment_id = $establishments[rand(1, $establishment_length) - 1]["establishment_id"];
	
	$sql = "
		INSERT INTO `hippodrome_contest`(
			`contest_id`,
			`establishment_id`,
			`hippodrome_contest_date_start`,
			`hippodrome_contest_date_end`)
		VALUES (
			$sql_contest_id,
			$sql_establishment_id,
			\"$sql_hippodrome_contest_date_start\",
			\"$sql_hippodrome_contest_date_end\");
	";

	print_r($sql);
	print_r("<br>");
}

