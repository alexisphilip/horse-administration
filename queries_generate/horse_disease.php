<?php

include("db_connect.php");


$sql = "SELECT disease_id FROM disease";
$diseases = $db->exec($sql);


$ids = [];

// Generates ~ 37,000 random numbers between 1 and 1,100,000 (~4%).
for ($i=0; $i < rand(33000, 40000); $i++) { 
	$ids[] = rand(1, 1100000);
}

$ids = implode(",", $ids);

// Selects the horse IDs from the randomly generated IDs.
$sql = "
	SELECT horse_id
	FROM horse
	WHERE horse_id IN ($ids);
";

$horse_ids = $db->exec($sql);


// For each horse IDs, add a disease.

$sql = "
	INSERT INTO `horse_disease`(
		`horse_id`,
		`disease_id`)
	VALUES
";

for ($i=0; $i < count($horse_ids); $i++) { 

	$sql_horse_id = $horse_ids[$i]["horse_id"];
	$sql_disease_id = $diseases[rand(1, count($diseases)) - 1]["disease_id"];

	$sql .= "
		($sql_horse_id,
		$sql_disease_id)
	";

	if ($i < count($horse_ids) -) {
		$sql .= ",";
	} else {
		$sql .= ";";
	}

}
