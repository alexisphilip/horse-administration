<?php

include("db_connect.php");


$sql = "SELECT injury_id FROM injury";
$injurys = $db->exec($sql);


$ids = [];

// Generates ~ 37,000 random numbers between 1 and 1,100,000 (~4%).
for ($i=0; $i < rand(80000, 90000); $i++) { 
	$ids[] = rand(1, 1100000);
}

$ids = implode(",", $ids);

// Selects the horse IDs from the randomly generated IDs.
$sql = "
	SELECT horse_id
	FROM horse
	WHERE horse_id IN ($ids);
";

$horse_ids = $db->exec($sql);


// For each horse IDs, add a injury.

$sql = "
	INSERT INTO `horse_injury`(
		`horse_id`,
		`injury_id`)
	VALUES
";

for ($i=0; $i < count($horse_ids); $i++) { 

	$sql_horse_id = $horse_ids[$i]["horse_id"];
	$sql_injury_id = $injurys[rand(1, count($injurys)) - 1]["injury_id"];

	$sql .= "
		($sql_horse_id,
		$sql_injury_id)
	";

	if ($i < count($horse_ids) - 1) {
		$sql .= ",";
	} else {
		$sql .= ";";
	}

}

print_r($sql);

