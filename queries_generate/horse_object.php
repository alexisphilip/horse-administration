<?php

include("db_connect.php");


$sql = "SELECT * FROM horse LIMIT 0, 10";
$horses = $db->exec($sql);


$sql = "SELECT o.*, ot.* FROM object o LEFT JOIN object_type ot ON ot.object_type_id = o.object_type_id";
$objects = $db->exec($sql);


$ai = 1;

// For each horses.
foreach ($horses as $key => $value) {

	// echo $value["horse_name"] . "<br>";

	// Numbers of objects assigned to these horses.
	if (rand(1, 6) === 1) {
		$rand_objects = rand(1, 4);
	} else {
		$rand_objects = rand(1, 2);
	}


	// Contains objects ID already taken.
	$objects_assigned = [];
	// Contains the objects types ID already taken.
	$type_assigned = [];

	// Assigns n objects (all must be different).
	for ($i=0; $i < $rand_objects; $i++) { 
		
		// Gets a valid "object" (must be a family "equipment" (id: 1) and not "food" (id: 2), and must not have already been taken).
		$continue = true;
		while ($continue) {
			$rand_object_id = rand(1, count($objects));
			$selected_object = $objects[$rand_object_id - 1];

			if ($selected_object["object_family_id"] == 1 &&
				!in_array($selected_object["object_id"], $objects_assigned) &&
				!in_array($selected_object["object_type_id"], $type_assigned)
			) {
				$objects_assigned[] = $selected_object["object_id"];
				$type_assigned[] = $selected_object["object_type_id"];
				$continue = false;
			}
		}
	}

	$sql = "
		INSERT INTO `horse_object`(
			`horse_id`,
			`object_id`)
		VALUES
	";

	// For each assigned objects.
	for ($i=0; $i < count($objects_assigned); $i++) { 
		$object_id = $objects_assigned[$i];

		$sql_auto_increment = $ai;
		$sql_horse_id = $value["horse_id"];
		$sql_object_id = $object_id;

		// print_r($sql_horse_id . " " . $sql_object_id . "<br>");

		$sql .= "
			($sql_horse_id,
			$sql_object_id)
		";

		if ($i < count($objects_assigned) - 1) {
			$sql .= ",";
		} else {
			$sql .= ";";
		}

		$ai++;
	}

	echo $sql;
	echo "<br>";
	// echo "<br><hr><br>";
}
