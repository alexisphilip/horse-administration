<?php

include("db_connect.php");


$sql = "SELECT parasite_id FROM parasite";
$parasites = $db->exec($sql);


$ids = [];

// Generates ~ 37,000 random numbers between 1 and 1,100,000 (~4%).
for ($i=0; $i < rand(50000, 60000); $i++) { 
	$ids[] = rand(1, 1100000);
}

$ids = implode(",", $ids);

// Selects the horse IDs from the randomly generated IDs.
$sql = "
	SELECT horse_id
	FROM horse
	WHERE horse_id IN ($ids);
";

$horse_ids = $db->exec($sql);


// For each horse IDs, add a parasite.

$sql = "
	INSERT INTO `horse_parasite`(
		`horse_id`,
		`parasite_id`)
	VALUES
";

for ($i=0; $i < count($horse_ids); $i++) { 

	$sql_horse_id = $horse_ids[$i]["horse_id"];
	$sql_parasite_id = $parasites[rand(1, count($parasites)) - 1]["parasite_id"];

	$sql .= "
		($sql_horse_id,
		$sql_parasite_id)
	";

	if ($i < count($horse_ids) - 1) {
		$sql .= ",";
	} else {
		$sql .= ";";
	}

}

print_r($sql);

