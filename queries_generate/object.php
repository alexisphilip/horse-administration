<?php

include("db_connect.php");


$sql = "SELECT * FROM object_type";
$object_types = $db->exec($sql);


$ai = 1;

// For each facilty families.
foreach ($object_types as $key => $value) {

	// Create 3 levels per objects.
	for ($i=1; $i <= 3; $i++) { 

		// FACILITY FAMILY
		$type_id = $value["object_type_id"];
		$type_name = $value["object_type_name"];

		// FACILITY LEVEL
		$level = $i;

		// FACILITY CAPACITY
		// FACILITY PRICE
		switch ($type_name) {
			case "bit":
				$price = 150 * $level;
				break;
			case "halter";
				$price = 200 * $level;
				break;
			case "harness";
				$price = 170 * $level;
				break;
			case "lead";
				$price = 60 * $level;
				break;
			case "saddle";
				$price = 850 * $level;
				break;
			case "pack_saddle";
				$price = 1100 * $level;
				break;
			case "collar";
				$price = 120 * $level;
				break;
			case "grains";
				$price = 70 * $level;
				break;
			case "oat";
				$price = 80 * $level;
				break;
			case "water";
				$price = 10 * $level;
				break;
			case "pature_grass";
				$price = 80 * $level;
				break;
			case "hay";
				$price = 60 * $level;
				break;
			case "mineral_block";
				$price = 50 * $level;
				break;
			case "salt_block";
				$price = 60 * $level;
				break;
		}

		// echo "[" . $type_id . "] " . $type_name . " (lvl:" . $level . ")";
		// echo "price: " . $price . "<br>";

		$sql_auto_increment = $type_id;
		$sql_object_type_id = $level;
		$sql_object_description = "NULL";
		$sql_object_price = $price;

		$sql = "
			INSERT INTO `object`(
				`object_type_id`,
				`object_level`,
				`object_description`,
				`object_price`)
			VALUES (
				$sql_auto_increment,
				$sql_object_type_id,
				$sql_object_description,
				$sql_object_price);
		";

		echo $sql;
		// echo "<br>";

		$ai++;
	}

	// echo "<hr><br>";
}
