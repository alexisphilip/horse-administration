<?php


include("db_connect.php");


$sql = "SELECT user_id FROM users";

$users = $db->exec($sql);

// For each users.
foreach ($users as $key => $user) {

	$random_ip = rand(1, 254) . "." . rand(1, 254) . "." . rand(1, 254) . "." . rand(1, 254);

	$sql = "
		INSERT INTO `user_log`(
			`user_id`,
			`user_history_ip`,
			`user_date_connection`)
		VALUES
	";
	
	// Generate bewteen 1 and 35 logs.
	$nb_logs = rand(1, 35);
	for ($y=0; $y < $nb_logs; $y++) {

		// Generate a random date between 2 dates.
		$timestamp_start = new DateTime("2008-01-01");
		$timestamp_end = new DateTime("2020-04-01");

		// Gets a random timestamp.
		$random_timestamp = rand($timestamp_start->getTimestamp(), $timestamp_end->getTimestamp());

		// Converts the random timestamp in date time.
		$sql_user_date_connection = date("Y-m-d H:i:s", $random_timestamp);

		$sql_user_id = $user["user_id"];

		// 1 / 7 the user will log into a new random IP.
		if (rand(1, 7) == 1) {
			$sql_user_history_ip = rand(1, 254) . "." . rand(1, 254) . "." . rand(1, 254) . "." . rand(1, 254);
		} else {
			$sql_user_history_ip = $random_ip;
		}

		$sql .= "
			($sql_user_id,
			\"$sql_user_history_ip\",
			\"$sql_user_date_connection\")
		";

		if ($y < $nb_logs - 1) {
			$sql .= ",";
		} else {
			$sql .= ";";
		}
	}

	print_r($sql);
	print_r("<br>");
}
